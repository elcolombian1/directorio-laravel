<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteServicio extends Model
{
    protected $table='cliente_servicio';

    protected $fillable=[
        'cliente_id',
        'servicio_id',
    ];

}
