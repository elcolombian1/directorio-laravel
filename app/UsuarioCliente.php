<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioCliente extends Model
{
    protected $fillable=['cliente_foreign','usuario_foreign'];

}
