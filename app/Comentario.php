<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{

    protected $fillable=[
        'contenido',
        'cliente_id',
        'usuario_id',
    ];


    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }
    public function usuario(){
        return $this->belongsTo('App\User');
    }

}
