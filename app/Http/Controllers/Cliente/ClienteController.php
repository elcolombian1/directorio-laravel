<?php

namespace App\Http\Controllers\Cliente;

use App\Comentario;
use App\Http\Requests\Cliente\StoreRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cliente;
use App\ClienteServicio;
use App\Departamento;
use App\Categoria;
use Illuminate\Support\Facades\DB;
use App\User;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($url)
    {
        $cliente=Cliente::where('url',$url)->first();
        return view('clientes.ver_perfil')->with(['empresa'=>$cliente,
                                                        'titulo'=>$cliente->nombre]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $crear_empresa=\Former::open()
            ->method('post')
            ->action(route('crear_clinica_post'))
            ->addClass('form')
            ->setAttribute('enctype','multipart/form-data');

        return view('clientes.crear_nuevo')->with(['formulario'=>$crear_empresa
                ,'servicios'=>\App\Servicio::all()
                ,'departamentos'=>Departamento::all()
                , 'categorias'=>Categoria::all()]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
                $cliente=Cliente::create($request->all()+
                            ['url'=>$request->input('nombre'),
                            'usuario_id'=>\Auth::user()->id]);

                $fotos_empresas_path=[];
                $counter=1;
                foreach ($request->file('fotos_empresa') as $foto){
                    array_push($fotos_empresas_path,$foto
                        ->storeAs('/fotos',$cliente->id.'-'.$counter.'.'.$foto->getClientOriginalExtension(),'public'));
                    $counter++;
                }

             $path_perfil=$request->file('foto_perfil')
                        ->storeAs('/perfiles',
                            $cliente->id.'.'.$request->file('foto_perfil')->getClientOriginalExtension(),'public');

                $cliente->update([
                    'foto_perfil'=>$path_perfil,
                    'fotos_cliente'=>json_encode($fotos_empresas_path),
                ]);


            foreach ($request->input('servicios') as $servicio)
            {
                ClienteServicio::create(
                    [
                        'cliente_id'=>$cliente->id,
                        'servicio_id'=>$servicio,
                    ]
                );
            }

            return redirect(route('ver_clinicas',['url'=>$cliente->url]));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function PerfilUsuario(){
                $user=\Auth::user();
                return(view('clientes.ver_perfil_usuario')
                        ->with('usuario',$user));
    }

    public function comentario(Request $request,Cliente $cliente){
            $validation=$request->validate([
                        'contenido'=>'required',
            ]);
            $cliente->comentarios()->save(new Comentario(array_merge($request->all(),['usuario_id'=>$request->user()->id])));
            return redirect(route('resultado',['url'=>$cliente->url]));
    }
}
