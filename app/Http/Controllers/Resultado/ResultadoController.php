<?php

namespace App\Http\Controllers\Resultado;

use App\Categoria;
use App\Cliente;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResultadoController extends Controller
{
    public function __invoke($url){
            $cliente=Cliente::where('url',$url)->first();
             $cliente->increment('visitas',1);
             $relacionados=Categoria::find($cliente->categoria_id)->clientes()->get();
            return(view('resultado.resultado')->with(['resultado'=>$cliente,'relacionados'=>$relacionados]));

    }
}
