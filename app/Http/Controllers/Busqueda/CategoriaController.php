<?php

namespace App\Http\Controllers\Busqueda;

use App\Categoria;
use App\Http\Controllers\Controller;


class CategoriaController extends Controller
{
    public function __invoke(Categoria $categoria){
        $resultados=$categoria->clientes()->get();
        return(view('busqueda.busqueda')->with('resultados',$resultados));

    }
}
