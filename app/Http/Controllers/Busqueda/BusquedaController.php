<?php

namespace App\Http\Controllers\Busqueda;

use App\Http\Controllers\Controller;
use App\Http\Requests\Busqueda\StoreRequest;
use App\Cliente;
class BusquedaController extends Controller
{
    public function __invoke(StoreRequest $request){


        $resultados=Cliente::where('nombre','like','%'.$request->input('nombre').'%')->get();

        return(view('busqueda.busqueda')->with('resultados',$resultados));

    }
}
