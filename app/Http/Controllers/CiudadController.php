<?php

namespace App\Http\Controllers;

use App\Departamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CiudadController extends Controller
{
    public function __invoke($id){
            $municipio=Departamento::find($id)->ciudades()->get();
            return response()->json($municipio);
    }
}
