<?php

namespace App\Http\Requests\Cliente;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'=>'unique:clientes|required|max:100',
            'foto_perfil'=>'image|max:5000',
            'fotos_cliente'=>'image|max:5000',
            'pagina_web'=>'unique:clientes',
        ];
    }
}
