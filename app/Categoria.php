<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{

      protected $fillable=[
        'nombre',
    ];

    public function setNombreAttribute($value){

        $this->attributes['nombre']=ucwords(strtolower($value));

    }

    public function scopeRecientes($query){

            return ($query->where('created_at','>=',now()
                            ->subsDays(8)
                    ));
    }

    public function clientes(){
        return $this->hasMany('App\Cliente');
    }
}
