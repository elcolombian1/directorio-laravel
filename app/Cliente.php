<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{

    protected $fillable=[

            'nombre',
            'usuario_id',
            'descripcion',
            'direccion',
            'ciudad_id',
            'categoria_id',
            'fotos_cliente',
            'foto_perfil',
            'url',
            'visitas',
            'telefono',
            'email_empresa',
            'pagina_web',


    ];



    public function getFotosClienteAttribute($value){


        return array_map(function($val)
                            {
                                return \Storage::disk('public')->url($val);
                            }

                            ,json_decode($value));

    }
    public function setNombreAttribuvate($value){
        $this->attributes['nombre']=ucwords(strtolower($value));

    }

    public function setDescripcionAttribute($value){
         $this->attributes['descripcion']=ucwords(strtolower($value));

    }


    public function getFotoPerfilAttribute($value){
            return \Storage::disk('public')->url($value);
    }

    public function  getPaginaWebAttribute($value){

            if(strpos($value,'http')){
                return($value);
            }
            else{
                return('https://'.$value);
            }

    }


    public function setUrlAttribute($value)
    {
        $this->attributes['url']=strtolower(str_replace(' ','-',self::eliminar_tildes(trim($value))));
    }

    public function categoria(){

        return $this->hasOne('App\Categoria');
    }

    public function  comentarios(){
        return $this->hasMany('App\Comentario');
    }

    public function servicios(){
        return $this->belongsToMany ('App\Servicio');
    }

    public function ciudad(){
        return $this->belongsTo('App\Ciudad');
    }

    public function usuario(){
        return $this->belongsTo('App\User','usuario_id');
    }



    public static function eliminar_tildes($cadena){

        //Codificamos la cadena en formato utf8 en caso de que nos de errores


        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena );

        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );

        return $cadena;
    }

}
