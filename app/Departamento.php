<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\clientes;
class Departamento extends Model
{

    protected $fillable=['nombre','slug'];

    public function ciudades(){
        return $this->hasMany('App\Ciudad','departamento_id');
    }
}
