<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $fillable=[
        'nombre',
        'cliente_foreign',


    ];
    protected $primaryKey='id';

    public function setNombreAttribute($value){

            $this->attributes['nombre']=ucwords(strtolower($value));
    }

    public function cliente_rel(){
        return $this->belongsToMany('clientes','id','cliente_foreign');
    }

}
