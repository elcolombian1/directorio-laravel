<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('clinicas')->middleware('auth')->group(function (){


        Route::get('crear_nueva','Cliente\ClienteController@create')->name('crear.clinica');
        Route::post('crear_nueva','Cliente\ClienteController@store')->name('crear_clinica_post');
        Route::get('editar/{id}','Cliente\ClienteController@edit');
        Route::post('editar/{id}','Cliente\ClienteController@update');
        Route::post('eliminar/{id}','Cliente\ClienteController@destroy');
        Route::get('/perfil/{url}','Cliente\ClienteController@index')->name('ver_clinicas');
        Route::post('comentario/{cliente}','Cliente\ClienteController@comentario')->name('comentario');

});


Route::get('/',function(){
        return view('index');
})->name('inicio');

Route::prefix('usuario')->middleware('guest')->group(function(){
    Route::get('ingresar','Auth\LoginController@ShowLoginForm');
    Route::get('registro','Auth\RegisterController@ShowRegistrationForm')->name('registro');
});

Route::get('perfil','Cliente\ClienteController@PerfilUsuario')->middleware('auth')->name('perfil');

Route::post('busqueda','Busqueda\BusquedaController')->name('busqueda');

Route::Auth();

Route::get('categoria/{categoria}','Busqueda\CategoriaController')->name('categoria.busqueda');
Route::get('/{url}','Resultado\ResultadoController')->name('resultado');

