<html>
@section('head')
<head>
    <link rel="stylesheet" type="text/css" href="css/clientes.css">
    <!-- Bootstrap Core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel='stylesheet' type='text/css' />

    <script src="https://kit.fontawesome.com/c6786b8721.js" crossorigin="anonymous"></script>
    <!-- //font-awesome icons CSS-->
    <link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">

</head>
@show
<body>
<div class="container-fluid" id="header">

    <div class="col-md-12 text-left color-white">
        <div class="row">
            <div class="col-md-2">
                <a href="{{route('inicio')}}"><img src="/images/logo.png" width="60%"></a>
            </div>

            <div class="col-md-10">
                @if(Auth::check())
                    <div class="text-right">
                        <p class="pt-2" style="font-family: 'Cairo"><a class="text-white" href="{{route('perfil')}}"><span class="fa fa-user"></span> {{Auth::user()['nombre']}}</a></p>
                    </div>
                @else
                    <a class="text-white" href="{{route('login')}}"><p><span class="fa fa-sign-in"></span> Iniciar sesion</p></a>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="container" >
    <div class="row mt-1" id="content">
        @section('content')
            <div class="col-md-6 offset-md-3 text-center bg-dark mt-5 p-5">
            <img src="/images/logo.png" width="30%">
                <h3 class="text-warning"> Amarillas de la salud</h3>
                        <hr>
                    <p class="text-warning"> ¿Que estas buscando?</p>
                <form class="form" action="{{route('busqueda')}}" method="post">
                    @csrf
                    <input type="text" name="nombre" class="form-control" placeholder="Ingresa el nombre que estas buscando...." required>
                    <br>
                    <button class="btn btn-lg btn-info" ><span class="fa fa-search"> </span> Buscar</button>
                </form>

                <p style="font-family: 'Cairo'" class="text-white">Encuentra todos los establecimientos que se encargan de cuidar tu salud, usa la barra de busqueda</p>
                <h1><span class="fa fa-stethoscope text-warning"></span> <span class="fa fa-medkit text-warning"></span>
                    <span class="fa fa-heartbeat text-warning"></span></h1>
                <hr>
                <div class="row">
                         @foreach(\App\Categoria::all() as $categoria)
                             <div class="col-md-2">
                                <a href="{{route('categoria.busqueda',['categoria'=>$categoria->id])}}"><p style="font-size: 12px">{{$categoria->nombre}}</p></a>
                             </div>
                         @endforeach
                </div>
            </div>
        @show

        @section('resultados')
            @show
    </div>
    </div>
</body>
</html>

