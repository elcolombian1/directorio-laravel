<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>@yield('titulo','Panel de usuario')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>


    <!-- Bootstrap Core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel='stylesheet' type='text/css' />

    <!-- Custom CSS -->
    <link href="/css/style.css" rel='stylesheet' type='text/css' />

    <!-- font-awesome icons CSS -->
    <link href="/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons CSS-->

    <!-- side nav css file -->
    <link href='/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
    <!-- //side nav css file -->

    <!-- js-->
    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/modernizr.custom.js"></script>

    <!--webfonts-->
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <!--//webfonts-->

    <!-- chart -->
    <script src="/js/Chart.js"></script>
    <!-- //chart -->

    <!-- Metis Menu -->
    <script src="/js/metisMenu.min.js"></script>
    <script src="/js/custom.js"></script>
    <link href="/css/custom.css" rel="stylesheet">

    <script src="/js/dropzone.js"></script>
    <link rel="stylesheet" href="/css/dropzone.css">

    @section('assets_locales')

    @show



</head>

<body class="cbp-spmenu-push">
<div class="main-content" style="background-color: #4287f5;">
@section('cabecera_lateral')

    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <!--left-fixed -navigation-->
        <aside class="sidebar-left">
            <nav class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{route('inicio')}}"><img src="/images/logo.png" width="100%"></a>

                </div>


                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-top:30%">
                    <ul class="sidebar-menu">
                        <li class="header">Menu Principal</li>
                        <li class="treeview">
                            @if(Auth::check() && Auth::user()->cliente)
                                    <a href="{{route('ver_clinicas',['url'=>Auth::user()->cliente['url']])}}">

                                <i class="fa fa-dashboard"></i> <span>Tu negocio</span>
                                </a>
                                @endif

                        </li>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
        </aside>
    </div>
    <!--left-fixed -navigation-->
@show
@section('barra_superior')
    <div class="header-right p-3" style="background-color:#3561a0">

        <div class="profile_details">
            <ul>
                <li class="dropdown profile_details_drop">
                    <a href="{{route('perfil')}}" >
                        <div class="profile_img">

                            <div class="user-name">
                                @if(Auth::check())
                                    <p class="text-white"><span class="fa fa-user text-white"></span><b> {{Auth::user()->nombre}}</b></p>
                                @else
                                    <a href="{{route('login')}}"><p>Entrar</p></a>
                                @endif
                                <span class="text-light">Usuario</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
</div>
<!-- //header-ends -->
<!-- main content start-->
@show

<div id="page-wrapper">
    @section('titulo_panel_interno')
        <h1> Crear nuevo negocio</h1>
    @show
    <div class="main-page">
        @section('indicadores_cajas')
        <div class="col_3">
            <div class="col-md-3 widget widget1">
                <div class="r3_counter_box">
                    <i class="pull-left fa fa-dollar icon-rounded"></i>
                    <div class="stats">
                        <h5><strong>$452</strong></h5>
                        <span>Total Revenue</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 widget widget1">
                <div class="r3_counter_box">
                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
                    <div class="stats">
                        <h5><strong>$1019</strong></h5>
                        <span>Online Revenue</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 widget widget1">
                <div class="r3_counter_box">
                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
                    <div class="stats">
                        <h5><strong>$1012</strong></h5>
                        <span>Expenses</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 widget widget1">
                <div class="r3_counter_box">
                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
                    <div class="stats">
                        <h5><strong>$450</strong></h5>
                        <span>Expenditure</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 widget">
                <div class="r3_counter_box">
                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
                    <div class="stats">
                        <h5><strong>1450</strong></h5>
                        <span>Total Users</span>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    @show
        <div class="row-one widgettable">
            @section('content')
                <div class="col-md-6 content-top-2 card">
                    <div class="agileinfo-cdr">
                        <div class="card-header">
                            <h3>Weekly Sales</h3>
                        </div>

                        <div id="Linegraph" style="width: 98%; height: 350px">
                        </div>

                    </div>
                </div>
            @show
            @section('cajas_datos')
            <div class="col-md-3 stat">
            </div>
                @show

            <div class="col-md-2 stat">
                @yield('redes_sociales')
            </div>
            <div class="clearfix"> </div>
        </div>



        <div class="footer">
            <p>Directorio medico de Villavicencio | Todo en un solo lugar</p>
        </div>
    </div>
</div>


</body>
</html>
