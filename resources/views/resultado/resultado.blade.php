@extends('index')
@section('head')
    @parent
@endsection
@section('content')
    @endsection
@section('resultados')
    <div class="col-md-10 offset-md-1 content-top stat card">
            <div class="agileinfo-cdr">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                        <img src="{{$resultado->foto_perfil}}" width="80%">
                        </div>
                        <div class="col-md-8 text-right">
                            <h1>{{$resultado->nombre}}</h1>
                            <p>  {{$resultado->descripcion}}</p>
                            <hr>
                            <p>
                                {{$resultado->direccion}} - {{$resultado->ciudad['nombre']}}
                            </p>
                            <a href="{{$resultado->pagina_web}}" target="_blank"> {{$resultado->pagina_web}} <span class="fa fa-external-link"></span></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    <hr>
                </div>
                <div class="card-fotter">
                    <p><b><span class="fa fa-database"></span> Esta empresa presta los siguientes servicios:</b></p>
                    <div class="row mx-4">
                        <div class="col-md-6 text-left bg-light p-3">

                        @foreach($resultado->servicios as $servicio)
                                <p><span class="fa fa-arrow-right"></span> {{$servicio->nombre}}</p>
                        @endforeach
                        </div>
                        <div class="col-md-4 offset-md-1 p-2 bg-light text-center pt-5">
                                <h6><span class="fa fa-phone"></span> {{$resultado->telefono}}</h6>
                                  <h6><span class="fa fa-envelope"></span> {{$resultado->email_empresa}}</h6>

                        </div>
                    </div>
                </div>
                <hr>
                <div class="col-md-6 offset-md-3">
                    <p><b>Imagenes de la empresa:</b></p>
                    <div class="row">
                        @foreach($resultado->fotos_cliente as $fotos)
                            <div class="col-md-4">
                                <img style="width:150px;height:100px;border:2px #303030 solid" src="{{$fotos}}" width="100%">
                            </div>
                        @endforeach
                    </div>
                </div>
                <hr>
                <p style="font-size: 10px">Visitado {{$resultado->visitas}} veces</p>
                <hr>
                <div class="row">
                          <div class="col-md-7 offset-md-1 p-2 ">
                              <div class="bg-light col-md-12 p-3">
                                 <h6>Comentarios</h6>
                                 <hr>
                                 @forelse($resultado->comentarios as $comentario)
                                     <div class="bg-white p-3 mb-2">
                                         <h6 class="text-secondary" style="font-family: 'Cairo'"><span class="fa fa-user"></span> {{$comentario->usuario->nombre}} <span style="font-size: 10px" class="text-dark">{!! $comentario->created_at !!}</span></h6>
                                         <p style="font-family: 'Arial';font-size: 12px" class="ml-3"><span class="fa fa-arrow-right"></span> {{$comentario->contenido}}</p>
                                     </div>
                                 @empty
                                     <div class="bg-white p-3 mb-2">
                                             <p class="text-center"><span class="fa fa-stack-overflow"></span> Esta empresa no tiene comentarios ¡Deja el tuyo!</p>
                                      </div>
                                 @endforelse
                              </div>
                          </div>
                    <div class="col-md-4 text-right">
                        <p><span class="fa fa-home"></span> Empresas similares</p>
                        @foreach($relacionados as $relacionado)
                            <a href="{{route('resultado',['url'=>$relacionado->url])}}"><img src="{{$relacionado->foto_perfil}}" width="30%"><p style="font-size: 10px">{{$relacionado->nombre}}</p></a>
                        @endforeach
                    </div>
                </div>
                <br>
                <div class="col-md-10">
                    <h4><span class="fa fa-pencil"></span> Deja tu comentario</h4>
                    <form method="post" class="form" action="{{route('comentario',['cliente'=>$resultado->id])}}">
                        @csrf
                        <label for="comentario" style="font-size: 12px;font-family: 'Cairo"> Deja tu comentario</label>
                        <textarea name="contenido" class="form-control"></textarea>
                         <br>
                         <button class="btn btn-info"><span class="fa fa-check"></span> Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    @endsection
