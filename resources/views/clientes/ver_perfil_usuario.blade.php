@extends('layouts.app')
@section('titulo')
    {{$usuario->nombre}}
@endsection

@section('panel_lateral')

@endsection

@section('indicadores_cajas')
@endsection

@section('titulo_panel_interno')

@endsection


@section('content')
        <div class="container-fluid bg-light">
            <div class="row">
                <div class="col-md-12 p-4 text-center">
                    <h1><span class="fa fa-users text-info"></span> {{$usuario->nombre}}</h1>
                </div>
                <div class="col-md-6 offset-md-3 text-center">
                    <div class="card">
                        <div class="card-header">
                            <h4>Tu negocio</h4>
                        </div>
                        <div class="card-body bg-white">
                            @if ($usuario->cliente)
                                <h1><span class="text-center fa fa-home text-success"></span></h1>
                                <h3><a href="{{route('ver_clinicas',['url'=>$usuario->cliente['url']])}}">{{$usuario->cliente['nombre']}}</a></h3>
                            @else
                                <h5 class="danger"> Este usuario no registra ningun negocio</h5>
                                <h6><a href="{{route('crear.clinica')}}"> Crear negocio</a></h6>
                            @endif
                        </div>

                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Cerrar sesion</h4>
                        </div>
                        <div class="card-body bg-white">
                               <h1> <form method="post" action="{{route('logout')}}">
                                       @csrf
                                       <button style="background-color: transparent">
                                           <span class="fa fa-sign-out text-warning"></span>
                                       </button>
                                   </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
