@extends('layouts.app')
@section('titulo')
    {{$titulo}}
@endsection

@section('panel_lateral')

@endsection

@section('indicadores_cajas')
@endsection

@section('titulo_panel_interno')

@endsection


@section('content')
<div class="row">
        <div class="col-md-7 content-top stat card">
            <div class="agileinfo-cdr">
                <div class="card-header">
                    <img src="{{$empresa->foto_perfil}}" width="30%">
                </div>
                <div class="card-body">
                    <h1>{{$empresa->nombre}}</h1>
                    <hr>
                </div>
                <div class="card-fotter">
                    <p><b>Tu empresa presta los siguientes servicios:</b></p>
                    <ol>
                    @foreach($empresa->servicios as $servicio)
                        <li><p>{{$servicio->nombre}}</p></li>
                    @endforeach
                    </ol>
                </div>
                <div class="col-md-12">
                    <p><b>Pagina web de tu empresa:</b></p>
                    <p>
                        <a href="{{$empresa->pagina_web}}" target="_blank"><p>{{$empresa->pagina_web}}</p> <span class="fa fa-external-link"></span></a>
                    </p>
                </div>
                <div class="col-md-12">
                    <p><b>La descripción de tu empresa:</b></p>
                    <p>
                        {{$empresa->descripcion}}
                    </p>
                </div>

                <div class="col-md-12">
                    <p><b>Tu empresa esta ubicada en:</b></p>
                    <p>
                        <b>{{$empresa->ciudad['nombre']}}</b>
                    </p>
                </div>

                <div class="col-md-12">
                    <hr>
                    <p><b>Calificación:</b></p>

                    <p>*La calificación maxima es de <b>5 estrellas</b></p>

                    @for($i=1;$i<=$empresa->calificacion;$i++)
                        <div class="col-md-2 col-md-offset-1">
                            <span class="fa fa-star" style="font-color:#303030"></span>
                        </div>

                    @endfor
                    <p style="text-align:center">Calificación actual {{$empresa->calificacion}} estrellas</p>
                    <hr>
                </div>
                   <div class="col-md-12">
                       <p><b>Fotos de tu empresa:</b></p>
                       <div class="row">
                       @foreach($empresa->fotos_cliente as $fotos)
                           <div class="col-md-4">
                               <img style="width:150px;height:100px;border:2px #303030 solid" src="{{$fotos}}" width="100%">

                           </div>
                       @endforeach
                       </div>
                   </div>
            </div>
        </div>
    <div class="col-md-4">
                @include('clientes.cajas_datos',['dato'=>'Visitas','icono'=>'fa fa-users','valor'=>$empresa->visitas])
                @include('clientes.cajas_datos',['dato'=>'Servicios','icono'=>'fa fa-database','valor'=>$empresa->servicios])
                @include('clientes.cajas_datos',['dato'=>'Direccion','icono'=>'fa fa-map-marker','valor'=>$empresa->direccion])
                @include('clientes.cajas_datos',['dato'=>'Telefono','icono'=>'fa fa-phone','valor'=>$empresa->telefono])
    </div>

</div>
@endsection





