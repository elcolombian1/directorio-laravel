
    <div class="col-md-12 content-top-1">
        <div class="card">
        <div class="card-header">
            <h3><b><span class="{{$icono}}"></span> {{$dato}}</b></h3>
        </div>
        <div class="card-body">
            @if(is_array($valor) || is_object($valor))
                <ol>
                 @for($i=0;$i<count($valor);$i++)
                     <li><b>{{$valor[$i]->nombre}}</b></li>
                @endfor
                </ol>
            @else
                    <p><b><span class="fa fa-arrow-right"></span> {{$valor}}</b></p>
            @endif

        </div>
        <div class="col-md-6 top-content1">
            <div id="demo-pie-1" class="pie-title-center" data-percent="45"> <span class="pie-value"></span> </div>
        </div>
        <div class="clearfix"> </div>
        </div>
        </div>


