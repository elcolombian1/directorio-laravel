@extends('layouts.app')
@section('titulo','Crear nuevo negocio')

    @section('panel_lateral')
        @parent
    @endsection

@section('indicadores_cajas')
    @endsection

@section('titulo_panel_interno')
    <h2> Crear nueva empresa <span class="fa fa-plus"></span></h2>
    @endsection

@section('assets_locales')
    <link rel="stylesheet" href="/css/crear_empresa.css">
    <script src="/js/drop_foto_perfil.js"></script>
    <script src="/js/municipios.js"></script>
@endsection
@section('content')
    <div class="col-md-12 content-top-2 card">
        <div class="agileinfo-cdr">
            <div class="col-md-8 col-md-offset-2">
                    <div class="card-header mb-2">
                        <h3 class="text-uppercase">Ingresa los datos de tu empresa</h3>
                    </div>
                <hr>
                    <div class="card-body">
                        {!!$formulario!!}

                        <label for="nombre">¿Cual es la razon social de tu empresa? *</label>
                         @error('nombre')
                            <p style="color:red">*Esta razon social ya fue registrada</p>
                         @enderror
                        <input type="text" name="nombre"  class="form-control" placeholder="Ingresa el nombre de tu empresa..." required="" value="{{old('nombre')}}">

                        <hr>
                        <label for="direccion">¿Donde esta tu empresa? *</label>
                        <input type="text" name="direccion"  class="form-control" placeholder="Ingresa la direccion de tu empresa..." required="" value="{{old('direccion')}}">
                        <hr>
                        <label for="email_empresa">¿Cual es el correo electronico de tu empresa? *</label>
                        <input type="email" name="email_empresa"  class="form-control" placeholder="Ingrese el correo electronico de tu empresa..." required="" value="{{old('email_empresa')}}">
                        <hr>
                        <label> Seleccione el departamento de ubicacion de tu empresa</label>
                        <select name="departamento" class="form-control"  onchange="get_municipios(this.value)" required="">
                            @foreach($departamentos as $departamento)
                                <option  value="{{$departamento->id}}">{{$departamento->nombre}} </option>
                            @endforeach
                        </select>
                        <hr>
                        <label> Seleccione el municipio de ubicacion de tu empresa</label>
                        <select name="ciudad_id" class="form-control text-uppercase" id="municipios" required=""">

                        </select>
                        <hr>
                            <label for="pagina_web">¿ Cual es la pagina web de tu empresa ?</label>
                        @error('pagina_web')
                            <p style="color:red">*Esta pagina web ya fue registrado</p>
                        @enderror
                        <input type="text" name="pagina_web" maxlength="50" class="form-control" value="{{old('pagina_web')}}">
                        <hr>
                        <label for="telefono">¿A que telefono pueden llamar a tu empresa? *</label>
                        <input type="text" maxlength="10" minlength="10" name="telefono"  class="form-control" placeholder="Ingresa el telefono de tu empresa..." required="" value="{{old('telefono')}}">
                        <hr>
                        <p><b>¿Que servicios presta tu empresa ? *</b></p>
                        <hr>
                        <div class="text-left">
                        @foreach($servicios as $servicio)
                                <div class="col-md-6">
                                <label class="container">{{$servicio->nombre}}
                                    <input type="checkbox"  name="servicios[]" value="{{$servicio->id}}">
                                    <span class="checkmark"></span>
                                </label>
                                </div>
                        @endforeach
                            <br>
                            <label for="categoria">Seleccione una categoria para tu empresa</label>
                            <select name="categoria_id" class="form-control" required="" value="{{old('categoria')}}">
                                <option></option>
                                @foreach($categorias as $categoria)
                                    <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                @endforeach
                            </select>
                            <label for="descripcion">Danos una breve descripción de lo que hace tu empresa</label>
                            <textarea name="descripcion"  class="form-control" rows="6" >{{old('descripcion')}}</textarea>
                            <br>
                            <label>Elije una foto de perfil para tu negocio *Puede ser el logotipo de tu empresa.</label>
                            @error('foto_perfil')
                                <p style="color:red">*Los archivos cargados no son valida, por favor verifique el tamaño y la extension</p>
                            @enderror
                            <input type="file" name="foto_perfil" maxlength="5MB" id="input_file" data-multiple-caption="{count} files selected" required value="{{old('foto_perfil')}}">

                            <label>Sube algunas fotografias de tu empresa.</label>
                            @error('fotos_cliente')
                            <p style="color:red">*Los archivos cargados no son valida, por favor verifique el tamaño y la extension</p>
                            @enderror
                            <input type="file" name="fotos_empresa[]" maxlength="200MB" max="4" id="input_file_" data-multiple-caption="{count} files selected" multiple value="{{old('fotos_cliente')}}">

                            <button type="submit" class="btn btn-info" style="margin-top:20px"><span class="fa fa-check"></span> Crear Empresa</button>
                        </div>



                        {!!$formulario->close()!!}

                    </div>


                    <div id="Linegraph" style="width: 98%; height: 350px">
                    </div>
            </div>
        </div>
    </div>

    @endsection
