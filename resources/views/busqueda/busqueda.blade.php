@extends('index')
@section('content')
@endsection
@section('resultados')
        <div class="col-md-12">
            <table class="table table-striped">
                @forelse($resultados as $resultado)
                <tr>
                    <td><img src="{{$resultado->foto_perfil}}" width="150px" height="100px"></td>
                    <td>
                        <b><span class="fa fa-home text-warning"></span> {{$resultado->nombre}}</b>
                            <ul>
                                    @foreach($resultado->servicios as $servicios)
                                        <li>{{$servicios->nombre}}</li>
                                    @endforeach
                            </ul>
                        <p class="text-center"><span class="fa fa-map-marker"></span> Ciudad: <b>{{$resultado->ciudad['nombre']}}</b></p>
                    </td>
                    <td>
                       <h1 class="text-success"><a href="{{route('resultado',['url'=>$resultado->url])}}"><span class="fa fa-arrow-right"></span></a></h1>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td>
                            <h1><span class="fa fa-stack-overflow text-warning"></span> No se encontraron resultados para esta busqueda</h1>
                        </td>
                    </tr>
                    @endforelse
            </table>
@endsection
