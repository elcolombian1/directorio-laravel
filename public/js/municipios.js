function get_municipios(id) {
    $.ajax({
            url: '/api/municipios/'+id,
            dataType: 'json',
            method: 'get',
            success: (value) => {
                $('#municipios').html('');
                value.forEach((val)=>{
                    $('#municipios').append('<option class="text-uppercase" value="'+val.id+'">'+val.nombre+'</option>"');
                })
            }
        }
    )
}
