<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->index();
            $table->text('descripcion');
            $table->string('direccion');
            $table->integer('calificacion');
            $table->longText('fotos_cliente')->nullable();
            $table->string('telefono',15);
            $table->string('email_empresa');
            $table->string('foto_perfil',100)->nullable();
            $table->integer('calificacion')->nullable();
            $table->integer('visitas')->nullable();
            $table->string('url',60)->unique();
            $table->string('pagina_web',50)->nullable();
            $table->unsignedBigInteger('ciudad_id');
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('categoria_id');

            $table->foreign('categoria_id')->references('id')
                ->on('categorias');

            $table->foreign('usuario_id')->references('id')
                        ->on('usuarios');

            $table->foreign('ciudad_id')
                ->references('id')
                ->on('ciudades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
