<?php

use Illuminate\Database\Seeder;
use App\Categoria;
class CategoriasSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias=[
            ['nombre'=>'Clinica'],
            ['nombre'=>'Consultorio'],
            ['nombre'=>'IPS'],
            ['nombre'=>'EPS'],
        ];

        foreach ($categorias as $categoria)
        {
            Categoria::updateOrCreate($categoria);
        }
    }
}
