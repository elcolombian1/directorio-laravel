<?php

use App\Departamento;
use Illuminate\Database\Seeder;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void_department
     */
    public function run()
    {


        $deparments = [
            ['nombre' => 'Amazonas', 'slug' => 'amazonas'],
            ['nombre' => 'Antioquia', 'slug' => 'antioquia'],
            ['nombre' => 'Arauca', 'slug' => 'arauca'],
            ['nombre' => 'Atlántico', 'slug' => 'atlantico'],
            ['nombre' => 'Bolívar', 'slug' => 'bolivar'],
            ['nombre' => 'Boyacá', 'slug' => 'boyaca'],
            ['nombre' => 'Caldas', 'slug' => 'caldas'],
            ['nombre' => 'Caquetá', 'slug' => 'caqueta'],
            ['nombre' => 'Casanare', 'slug' => 'casanare'],
            ['nombre' => 'Cauca', 'slug' => 'cauca'],
            ['nombre' => 'Cesar', 'slug' => 'cesar'],
            ['nombre' => 'Chocó', 'slug' => 'choco'],
            ['nombre' => 'Córdoba', 'slug' => 'cordoba'],
            ['nombre' => 'Cundinamarca', 'slug' => 'cundinamarca'],
            ['nombre' => 'Distrito Capital de Bogotá', 'slug' => 'distrito-capital-de-bogota'],
            ['nombre' => 'Guainía', 'slug' => 'guainia'],
            ['nombre' => 'Guaviare', 'slug' => 'guaviare'],
            ['nombre' => 'Huila', 'slug' => 'huila'],
            ['nombre' => 'La Guajira', 'slug' => 'la-guajira'],
            ['nombre' => 'Magdalena', 'slug' => 'magdalena'],
            ['nombre' => 'Meta', 'slug' => 'meta'],
            ['nombre' => 'Nariño', 'slug' => 'narino'],
            ['nombre' => 'Norte de Santander', 'slug' => 'norte-de-santander'],
            ['nombre' => 'Provid_departmentencia y Santa Catalina, Archipiélago de San Andrés', 'slug' => 'provid_departmentencia-y-santa-catalina-archipielago-de-san-andres'],
            ['nombre' => 'Putumayo', 'slug' => 'putumayo'],
            ['nombre' => 'Quindío', 'slug' => 'quindio'],
            ['nombre' => 'Risaralda', 'slug' => 'risaralda'],
            ['nombre' => 'Santander', 'slug' => 'santander'],
            ['nombre' => 'Sucre', 'slug' => 'sucre'],
            ['nombre' => 'Tolima', 'slug' => 'tolima'],
            ['nombre' => 'Valle del Cauca', 'slug' => 'valle-del-cauca'],
            ['nombre' => 'Vaupés', 'slug' => 'vaupes'],
            ['nombre' => 'Vichada', 'slug' => 'vichada'],
        ];

        foreach ($deparments as $deparment)
        {
            Departamento::create($deparment);
        }

    }
}
