<?php

use App\Cuidad;
use App\Departamento;
use Illuminate\Database\Seeder;

class CiudadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void_city
     */
    public function run()
    {
        $department = Departamento::where(['slug' => 'amazonas'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Tarapacá', 'slug' => 'tarapaca'],
            ['nombre' => 'Puerto Alegría', 'slug' => 'puerto-alegria'],
            ['nombre' => 'Mirití Paraná', 'slug' => 'miriti-parana'],
            ['nombre' => 'Leticia', 'slug' => 'leticia'],
            ['nombre' => 'La Pedrera', 'slug' => 'la-pedrera'],
            ['nombre' => 'La Chorrera', 'slug' => 'la-chorrera'],
            ['nombre' => 'El Encanto', 'slug' => 'el-encanto'],
            ['nombre' => 'Puerto Santander', 'slug' => 'puerto-santander'],
            ['nombre' => 'Puerto Arica', 'slug' => 'puerto-arica'],
            ['nombre' => 'La Victoria', 'slug' => 'la-victoria'],
            ['nombre' => 'Puerto Nariño', 'slug' => 'puerto-narino'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'antioquia'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Yolombó', 'slug' => 'yolombo'],
            ['nombre' => 'Yarumal', 'slug' => 'yarumal'],
            ['nombre' => 'Valdivia', 'slug' => 'valdivia'],
            ['nombre' => 'Tarazá', 'slug' => 'taraza'],
            ['nombre' => 'Sopetrán', 'slug' => 'sopetran'],
            ['nombre' => 'San Vicente', 'slug' => 'san-vicente'],
            ['nombre' => 'Santa Bárbara', 'slug' => 'santa-barbara'],
            ['nombre' => 'San Miguel', 'slug' => 'san-miguel'],
            ['nombre' => 'San Jerónimo', 'slug' => 'san-jeronimo'],
            ['nombre' => 'Sabaneta', 'slug' => 'sabaneta'],
            ['nombre' => 'Retiro', 'slug' => 'retiro'],
            ['nombre' => 'Puerto Berrío', 'slug' => 'puerto-berrio'],
            ['nombre' => 'Olaya', 'slug' => 'olaya'],
            ['nombre' => 'Marinilla', 'slug' => 'marinilla'],
            ['nombre' => 'Puerto Nare', 'slug' => 'puerto-nare'],
            ['nombre' => 'La Estrella', 'slug' => 'la-estrella'],
            ['nombre' => 'Guarne', 'slug' => 'guarne'],
            ['nombre' => 'Estación Angelópolis', 'slug' => 'estacion-angelopolis'],
            ['nombre' => 'Entrerríos', 'slug' => 'entrerrios'],
            ['nombre' => 'Dabeiba', 'slug' => 'dabeiba'],
            ['nombre' => 'Córdoba', 'slug' => 'cordoba'],
            ['nombre' => 'Cisneros', 'slug' => 'cisneros'],
            ['nombre' => 'Cañasgordas', 'slug' => 'canasgordas'],
            ['nombre' => 'Caicedo', 'slug' => 'caicedo'],
            ['nombre' => 'Cáceres', 'slug' => 'caceres'],
            ['nombre' => 'Anzá', 'slug' => 'anza'],
            ['nombre' => 'Antioquia', 'slug' => 'antioquia'],
            ['nombre' => 'Angostura', 'slug' => 'angostura'],
            ['nombre' => 'El Bagre', 'slug' => 'el-bagre'],
            ['nombre' => 'Campamento', 'slug' => 'campamento'],
            ['nombre' => 'Fredonia', 'slug' => 'fredonia'],
            ['nombre' => 'Montebello', 'slug' => 'montebello'],
            ['nombre' => 'San Rafael', 'slug' => 'san-rafael'],
            ['nombre' => 'Alejandría', 'slug' => 'alejandria'],
            ['nombre' => 'Jardin', 'slug' => 'jardin'],
            ['nombre' => 'La Ceja', 'slug' => 'la-ceja'],
            ['nombre' => 'La Unión', 'slug' => 'la-union'],
            ['nombre' => 'San Carlos', 'slug' => 'san-carlos'],
            ['nombre' => 'San Francisco', 'slug' => 'san-francisco'],
            ['nombre' => 'San Pedro', 'slug' => 'san-pedro'],
            ['nombre' => 'San Roque', 'slug' => 'san-roque'],
            ['nombre' => 'Santo Domingo', 'slug' => 'santo-domingo'],
            ['nombre' => 'Venecia', 'slug' => 'venecia'],
            ['nombre' => 'Betania', 'slug' => 'betania'],
            ['nombre' => 'Girardota', 'slug' => 'girardota'],
            ['nombre' => 'Carolina del Príncipe', 'slug' => 'carolina-del-principe'],
            ['nombre' => 'San Andres Y Providencia', 'slug' => 'san-andres-y-providencia'],
            ['nombre' => 'Yondó Apartadó', 'slug' => 'yondo-apartado'],
            ['nombre' => 'Zaragoza Caracolí', 'slug' => 'zaragoza-caracoli'],
            ['nombre' => 'Abriaquí', 'slug' => 'abriaqui'],
            ['nombre' => 'Belmira', 'slug' => 'belmira'],
            ['nombre' => 'Giraldo', 'slug' => 'giraldo'],
            ['nombre' => 'Concepción', 'slug' => 'concepcion'],
            ['nombre' => 'Guadalupe', 'slug' => 'guadalupe'],
            ['nombre' => 'Peque', 'slug' => 'peque'],
            ['nombre' => 'Armenia', 'slug' => 'armenia'],
            ['nombre' => 'Briceño', 'slug' => 'briceno'],
            ['nombre' => 'Uramita', 'slug' => 'uramita'],
            ['nombre' => 'Peñol', 'slug' => 'penol'],
            ['nombre' => 'Murindó', 'slug' => 'murindo'],
            ['nombre' => 'Liborina', 'slug' => 'liborina'],
            ['nombre' => 'Buriticá', 'slug' => 'buritica'],
            ['nombre' => 'Heliconia', 'slug' => 'heliconia'],
            ['nombre' => 'Hispania', 'slug' => 'hispania'],
            ['nombre' => 'Sabanalarga', 'slug' => 'sabanalarga'],
            ['nombre' => 'Ebéjico', 'slug' => 'ebejico'],
            ['nombre' => 'Tarso', 'slug' => 'tarso'],
            ['nombre' => 'San José de La Montaña', 'slug' => 'san-jose-de-la-montana'],
            ['nombre' => 'Caramanta', 'slug' => 'caramanta'],
            ['nombre' => 'Maceo', 'slug' => 'maceo'],
            ['nombre' => 'San Andrés', 'slug' => 'san-andres'],
            ['nombre' => 'Nariño', 'slug' => 'narino'],
            ['nombre' => 'Argelia', 'slug' => 'argelia'],
            ['nombre' => 'Valparaíso', 'slug' => 'valparaiso'],
            ['nombre' => 'Titiribí', 'slug' => 'titiribi'],
            ['nombre' => 'Yalí', 'slug' => 'yali'],
            ['nombre' => 'Puerto Triunfo', 'slug' => 'puerto-triunfo'],
            ['nombre' => 'Gómez Plata', 'slug' => 'gomez-plata'],
            ['nombre' => 'Pueblorrico', 'slug' => 'pueblorrico'],
            ['nombre' => 'Concordia', 'slug' => 'concordia'],
            ['nombre' => 'Vigía del Fuerte', 'slug' => 'vigia-del-fuerte'],
            ['nombre' => 'Cruces de Anorí', 'slug' => 'cruces-de-anori'],
            ['nombre' => 'Mutatá', 'slug' => 'mutata'],
            ['nombre' => 'Betulia', 'slug' => 'betulia'],
            ['nombre' => 'La Pintada', 'slug' => 'la-pintada'],
            ['nombre' => 'Guatapé', 'slug' => 'guatape'],
            ['nombre' => 'Granada', 'slug' => 'granada'],
            ['nombre' => 'Támesis', 'slug' => 'tamesis'],
            ['nombre' => 'Remedios', 'slug' => 'remedios'],
            ['nombre' => 'San Luis', 'slug' => 'san-luis'],
            ['nombre' => 'Nechí', 'slug' => 'nechi'],
            ['nombre' => 'Abejorral', 'slug' => 'abejorral'],
            ['nombre' => 'Jericó', 'slug' => 'jerico'],
            ['nombre' => 'Arboletes', 'slug' => 'arboletes'],
            ['nombre' => 'Frontino', 'slug' => 'frontino'],
            ['nombre' => 'Vegachí', 'slug' => 'vegachi'],
            ['nombre' => 'Amalfi', 'slug' => 'amalfi'],
            ['nombre' => 'Salgar', 'slug' => 'salgar'],
            ['nombre' => 'Don Matías', 'slug' => 'don-matias'],
            ['nombre' => 'Santa Rosa de Osos', 'slug' => 'santa-rosa-de-osos'],
            ['nombre' => 'Ituango', 'slug' => 'ituango'],
            ['nombre' => 'San Juan de Urabá', 'slug' => 'san-juan-de-uraba'],
            ['nombre' => 'Necoclí', 'slug' => 'necocli'],
            ['nombre' => 'San Pedro de Urabá', 'slug' => 'san-pedro-de-uraba'],
            ['nombre' => 'Segovia', 'slug' => 'segovia'],
            ['nombre' => 'Amagá', 'slug' => 'amaga'],
            ['nombre' => 'Ciudad Bolívar', 'slug' => 'ciudad-bolivar'],
            ['nombre' => 'Andes', 'slug' => 'andes'],
            ['nombre' => 'Barbosa', 'slug' => 'barbosa'],
            ['nombre' => 'Sonsón', 'slug' => 'sonson'],
            ['nombre' => 'Santuario', 'slug' => 'santuario'],
            ['nombre' => 'Urrao', 'slug' => 'urrao'],
            ['nombre' => 'Carepa', 'slug' => 'carepa'],
            ['nombre' => 'Carmen de Viboral', 'slug' => 'carmen-de-viboral'],
            ['nombre' => 'Chigorodó', 'slug' => 'chigorodo'],
            ['nombre' => 'Copacabana', 'slug' => 'copacabana'],
            ['nombre' => 'Turbo', 'slug' => 'turbo'],
            ['nombre' => 'Caucasia', 'slug' => 'caucasia'],
            ['nombre' => 'Rionegro', 'slug' => 'rionegro'],
            ['nombre' => 'Caldas', 'slug' => 'caldas'],
            ['nombre' => 'Envigado', 'slug' => 'envigado'],
            ['nombre' => 'Itagüí', 'slug' => 'itagui'],
            ['nombre' => 'Bello', 'slug' => 'bello'],
            ['nombre' => 'Medellín', 'slug' => 'medellin'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'arauca'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Saravena', 'slug' => 'saravena'],
            ['nombre' => 'Arauca', 'slug' => 'arauca'],
            ['nombre' => 'Fortul', 'slug' => 'fortul'],
            ['nombre' => 'Puerto Rondón', 'slug' => 'puerto-rondon'],
            ['nombre' => 'Cravo Norte', 'slug' => 'cravo-norte'],
            ['nombre' => 'Arauquita', 'slug' => 'arauquita'],
            ['nombre' => 'Tame', 'slug' => 'tame'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'atlantico'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Soledad', 'slug' => 'soledad'],
            ['nombre' => 'Santo Tomás', 'slug' => 'santo-tomas'],
            ['nombre' => 'Santa Lucía', 'slug' => 'santa-lucia'],
            ['nombre' => 'Sabanagrande', 'slug' => 'sabanagrande'],
            ['nombre' => 'Repelón', 'slug' => 'repelon'],
            ['nombre' => 'Manatí', 'slug' => 'manati'],
            ['nombre' => 'Malambo', 'slug' => 'malambo'],
            ['nombre' => 'Candelaria', 'slug' => 'candelaria'],
            ['nombre' => 'Baranoa', 'slug' => 'baranoa'],
            ['nombre' => 'Piojó', 'slug' => 'piojo'],
            ['nombre' => 'Tubará', 'slug' => 'tubara'],
            ['nombre' => 'Juan de Acosta', 'slug' => 'juan-de-acosta'],
            ['nombre' => 'Usiacurí', 'slug' => 'usiacuri'],
            ['nombre' => 'Suan', 'slug' => 'suan'],
            ['nombre' => 'Ponedera', 'slug' => 'ponedera'],
            ['nombre' => 'Polonuevo', 'slug' => 'polonuevo'],
            ['nombre' => 'Luruaco', 'slug' => 'luruaco'],
            ['nombre' => 'Galapa', 'slug' => 'galapa'],
            ['nombre' => 'Campo de la Cruz', 'slug' => 'campo-de-la-cruz'],
            ['nombre' => 'Puerto Colombia', 'slug' => 'puerto-colombia'],
            ['nombre' => 'Palmar de Varela', 'slug' => 'palmar-de-varela'],
            ['nombre' => 'Sabanalarga', 'slug' => 'sabanalarga'],
            ['nombre' => 'Barranquilla', 'slug' => 'barranquilla'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'bolivar'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Zambrano', 'slug' => 'zambrano'],
            ['nombre' => 'Turbaco', 'slug' => 'turbaco'],
            ['nombre' => 'Santa Rosa', 'slug' => 'santa-rosa'],
            ['nombre' => 'Santa Catalina', 'slug' => 'santa-catalina'],
            ['nombre' => 'San Pablo', 'slug' => 'san-pablo'],
            ['nombre' => 'San Martín de Loba', 'slug' => 'san-martin-de-loba'],
            ['nombre' => 'San Jacinto de Achí', 'slug' => 'san-jacinto-de-achi'],
            ['nombre' => 'San Jacinto', 'slug' => 'san-jacinto'],
            ['nombre' => 'San Fernando', 'slug' => 'san-fernando'],
            ['nombre' => 'San Cristóbal', 'slug' => 'san-cristobal'],
            ['nombre' => 'Río Viejo', 'slug' => 'rio-viejo'],
            ['nombre' => 'Montecristo', 'slug' => 'montecristo'],
            ['nombre' => 'Margarita', 'slug' => 'margarita'],
            ['nombre' => 'El Guamo', 'slug' => 'el-guamo'],
            ['nombre' => 'Cantagallo', 'slug' => 'cantagallo'],
            ['nombre' => 'Calamar', 'slug' => 'calamar'],
            ['nombre' => 'Arjona', 'slug' => 'arjona'],
            ['nombre' => 'Soplaviento', 'slug' => 'soplaviento'],
            ['nombre' => 'Arenal del Sur', 'slug' => 'arenal-del-sur'],
            ['nombre' => 'Bello Horizonte', 'slug' => 'bello-horizonte'],
            ['nombre' => 'Isla Baru', 'slug' => 'isla-baru'],
            ['nombre' => 'Santa Cruz de Mompox', 'slug' => 'santa-cruz-de-mompox'],
            ['nombre' => 'Talaiga Nuevo', 'slug' => 'talaiga-nuevo'],
            ['nombre' => 'Regidor', 'slug' => 'regidor'],
            ['nombre' => 'Arroyohondo', 'slug' => 'arroyohondo'],
            ['nombre' => 'Hatillo de Loba', 'slug' => 'hatillo-de-loba'],
            ['nombre' => 'El Peñón', 'slug' => 'el-penon'],
            ['nombre' => 'Tiquisio', 'slug' => 'tiquisio'],
            ['nombre' => 'Altos del Rosario', 'slug' => 'altos-del-rosario'],
            ['nombre' => 'Barranco de Loba', 'slug' => 'barranco-de-loba'],
            ['nombre' => 'Pinillos', 'slug' => 'pinillos'],
            ['nombre' => 'Córdoba', 'slug' => 'cordoba'],
            ['nombre' => 'Simití', 'slug' => 'simiti'],
            ['nombre' => 'Cicuco', 'slug' => 'cicuco'],
            ['nombre' => 'Achí', 'slug' => 'achi'],
            ['nombre' => 'Clemencia', 'slug' => 'clemencia'],
            ['nombre' => 'Santa Rosa del Sur', 'slug' => 'santa-rosa-del-sur'],
            ['nombre' => 'Mahates', 'slug' => 'mahates'],
            ['nombre' => 'Turbaná', 'slug' => 'turbana'],
            ['nombre' => 'San Estanislao', 'slug' => 'san-estanislao'],
            ['nombre' => 'Villanueva', 'slug' => 'villanueva'],
            ['nombre' => 'María la Baja', 'slug' => 'maria-la-baja'],
            ['nombre' => 'Morales', 'slug' => 'morales'],
            ['nombre' => 'San Juan Nepomuceno', 'slug' => 'san-juan-nepomuceno'],
            ['nombre' => 'El Carmen de Bolívar', 'slug' => 'el-carmen-de-bolivar'],
            ['nombre' => 'Magangué', 'slug' => 'magangue'],
            ['nombre' => 'Cartagena', 'slug' => 'cartagena'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'boyaca'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'La Victoria', 'slug' => 'la-victoria'],
            ['nombre' => 'Ventaquemada', 'slug' => 'ventaquemada'],
            ['nombre' => 'Tunja', 'slug' => 'tunja'],
            ['nombre' => 'Tibasosa', 'slug' => 'tibasosa'],
            ['nombre' => 'Somondoco', 'slug' => 'somondoco'],
            ['nombre' => 'Soatá', 'slug' => 'soata'],
            ['nombre' => 'Santa Sofía', 'slug' => 'santa-sofia'],
            ['nombre' => 'Santana', 'slug' => 'santana'],
            ['nombre' => 'San Pablo de Borbur', 'slug' => 'san-pablo-de-borbur'],
            ['nombre' => 'Rondón', 'slug' => 'rondon'],
            ['nombre' => 'Puerto Boyacá', 'slug' => 'puerto-boyaca'],
            ['nombre' => 'Pesca', 'slug' => 'pesca'],
            ['nombre' => 'Paz de Río', 'slug' => 'paz-de-rio'],
            ['nombre' => 'Moniquirá', 'slug' => 'moniquira'],
            ['nombre' => 'Monguí', 'slug' => 'mongui'],
            ['nombre' => 'Guacamayas', 'slug' => 'guacamayas'],
            ['nombre' => 'Floresta', 'slug' => 'floresta'],
            ['nombre' => 'El Espino', 'slug' => 'el-espino'],
            ['nombre' => 'El Cocuy', 'slug' => 'el-cocuy'],
            ['nombre' => 'Corrales', 'slug' => 'corrales'],
            ['nombre' => 'Cómbita', 'slug' => 'combita'],
            ['nombre' => 'Chita', 'slug' => 'chita'],
            ['nombre' => 'Chinavita', 'slug' => 'chinavita'],
            ['nombre' => 'Cerinza', 'slug' => 'cerinza'],
            ['nombre' => 'Campohermoso', 'slug' => 'campohermoso'],
            ['nombre' => 'Boyacá', 'slug' => 'boyaca'],
            ['nombre' => 'La Capilla', 'slug' => 'la-capilla'],
            ['nombre' => 'Macanal', 'slug' => 'macanal'],
            ['nombre' => 'Otanche', 'slug' => 'otanche'],
            ['nombre' => 'Cucaita', 'slug' => 'cucaita'],
            ['nombre' => 'Miraflores', 'slug' => 'miraflores'],
            ['nombre' => 'Buenavista', 'slug' => 'buenavista'],
            ['nombre' => 'San Mateo', 'slug' => 'san-mateo'],
            ['nombre' => 'Chitaraque', 'slug' => 'chitaraque'],
            ['nombre' => 'Pisba Berbeo', 'slug' => 'pisba-berbeo'],
            ['nombre' => 'Busbanzá', 'slug' => 'busbanza'],
            ['nombre' => 'Tununguá', 'slug' => 'tunungua'],
            ['nombre' => 'Cuítiva', 'slug' => 'cuitiva'],
            ['nombre' => 'Berbeo', 'slug' => 'berbeo'],
            ['nombre' => 'Tutasá', 'slug' => 'tutasa'],
            ['nombre' => 'Oicatá', 'slug' => 'oicata'],
            ['nombre' => 'Paya', 'slug' => 'paya'],
            ['nombre' => 'Betéitiva', 'slug' => 'beteitiva'],
            ['nombre' => 'Motavita', 'slug' => 'motavita'],
            ['nombre' => 'Tinjacá', 'slug' => 'tinjaca'],
            ['nombre' => 'Caldas', 'slug' => 'caldas'],
            ['nombre' => 'Sora', 'slug' => 'sora'],
            ['nombre' => 'Gachantiva', 'slug' => 'gachantiva'],
            ['nombre' => 'Viracachá', 'slug' => 'viracacha'],
            ['nombre' => 'Sativasur', 'slug' => 'sativasur'],
            ['nombre' => 'Covarachía', 'slug' => 'covarachia'],
            ['nombre' => 'Briceño', 'slug' => 'briceno'],
            ['nombre' => 'San Miguel de Sema', 'slug' => 'san-miguel-de-sema'],
            ['nombre' => 'Chivatá', 'slug' => 'chivata'],
            ['nombre' => 'Panqueba', 'slug' => 'panqueba'],
            ['nombre' => 'Chíquiza', 'slug' => 'chiquiza'],
            ['nombre' => 'Iza', 'slug' => 'iza'],
            ['nombre' => 'Almeida', 'slug' => 'almeida'],
            ['nombre' => 'Tota', 'slug' => 'tota'],
            ['nombre' => 'Sativanorte', 'slug' => 'sativanorte'],
            ['nombre' => 'Coper', 'slug' => 'coper'],
            ['nombre' => 'Soracá', 'slug' => 'soraca'],
            ['nombre' => 'Sutatenza', 'slug' => 'sutatenza'],
            ['nombre' => 'Togüí', 'slug' => 'togui'],
            ['nombre' => 'Nuevo Colón', 'slug' => 'nuevo-colon'],
            ['nombre' => 'Jericó', 'slug' => 'jerico'],
            ['nombre' => 'Labranzagrande', 'slug' => 'labranzagrande'],
            ['nombre' => 'San José de Pare', 'slug' => 'san-jose-de-pare'],
            ['nombre' => 'Pachavita', 'slug' => 'pachavita'],
            ['nombre' => 'Tópaga', 'slug' => 'topaga'],
            ['nombre' => 'Tipacoque', 'slug' => 'tipacoque'],
            ['nombre' => 'Zetaquira', 'slug' => 'zetaquira'],
            ['nombre' => 'Maripí', 'slug' => 'maripi'],
            ['nombre' => 'Ciénega', 'slug' => 'cienega'],
            ['nombre' => 'Jenesano', 'slug' => 'jenesano'],
            ['nombre' => 'Susacón', 'slug' => 'susacon'],
            ['nombre' => 'Pajarito', 'slug' => 'pajarito'],
            ['nombre' => 'Úmbita', 'slug' => 'umbita'],
            ['nombre' => 'Tenza', 'slug' => 'tenza'],
            ['nombre' => 'Sotaquirá', 'slug' => 'sotaquira'],
            ['nombre' => 'Chiscas', 'slug' => 'chiscas'],
            ['nombre' => 'Siachoque', 'slug' => 'siachoque'],
            ['nombre' => 'Saboyá', 'slug' => 'saboya'],
            ['nombre' => 'Sutamarchán', 'slug' => 'sutamarchan'],
            ['nombre' => 'Cubará', 'slug' => 'cubara'],
            ['nombre' => 'Arcabuco', 'slug' => 'arcabuco'],
            ['nombre' => 'Chivor', 'slug' => 'chivor'],
            ['nombre' => 'Tuta', 'slug' => 'tuta'],
            ['nombre' => 'Gámeza', 'slug' => 'gameza'],
            ['nombre' => 'La Uvita', 'slug' => 'la-uvita'],
            ['nombre' => 'Sáchica', 'slug' => 'sachica'],
            ['nombre' => 'Tibaná', 'slug' => 'tibana'],
            ['nombre' => 'Tasco', 'slug' => 'tasco'],
            ['nombre' => 'Güicán', 'slug' => 'guican'],
            ['nombre' => 'Ráquira', 'slug' => 'raquira'],
            ['nombre' => 'Firavitoba', 'slug' => 'firavitoba'],
            ['nombre' => 'Socotá', 'slug' => 'socota'],
            ['nombre' => 'Santa María', 'slug' => 'santa-maria'],
            ['nombre' => 'Mongua', 'slug' => 'mongua'],
            ['nombre' => 'San Luis de Gaceno', 'slug' => 'san-luis-de-gaceno'],
            ['nombre' => 'Pauna', 'slug' => 'pauna'],
            ['nombre' => 'Guayatá', 'slug' => 'guayata'],
            ['nombre' => 'Turmequé', 'slug' => 'turmeque'],
            ['nombre' => 'Socha Viejo', 'slug' => 'socha-viejo'],
            ['nombre' => 'Nobsa', 'slug' => 'nobsa'],
            ['nombre' => 'Quípama', 'slug' => 'quipama'],
            ['nombre' => 'Samacá', 'slug' => 'samaca'],
            ['nombre' => 'Boavita', 'slug' => 'boavita'],
            ['nombre' => 'Toca', 'slug' => 'toca'],
            ['nombre' => 'Ramiriquí', 'slug' => 'ramiriqui'],
            ['nombre' => 'Villa De Leyva', 'slug' => 'villa-de-leyva'],
            ['nombre' => 'Belén', 'slug' => 'belen'],
            ['nombre' => 'Aquitania', 'slug' => 'aquitania'],
            ['nombre' => 'Santa Rosa de Viterbo', 'slug' => 'santa-rosa-de-viterbo'],
            ['nombre' => 'Guateque', 'slug' => 'guateque'],
            ['nombre' => 'Muzo', 'slug' => 'muzo'],
            ['nombre' => 'Garagoa', 'slug' => 'garagoa'],
            ['nombre' => 'Paipa', 'slug' => 'paipa'],
            ['nombre' => 'Páez', 'slug' => 'paez'],
            ['nombre' => 'Chiquinquirá', 'slug' => 'chiquinquira'],
            ['nombre' => 'Duitama', 'slug' => 'duitama'],
            ['nombre' => 'Sogamoso', 'slug' => 'sogamoso'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'caldas'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Villamaría', 'slug' => 'villamaria'],
            ['nombre' => 'Victoria', 'slug' => 'victoria'],
            ['nombre' => 'Samaná', 'slug' => 'samana'],
            ['nombre' => 'Risaralda', 'slug' => 'risaralda'],
            ['nombre' => 'Pensilvania', 'slug' => 'pensilvania'],
            ['nombre' => 'Pácora', 'slug' => 'pacora'],
            ['nombre' => 'Norcasia', 'slug' => 'norcasia'],
            ['nombre' => 'Marquetalia', 'slug' => 'marquetalia'],
            ['nombre' => 'Marmato', 'slug' => 'marmato'],
            ['nombre' => 'Manzanares', 'slug' => 'manzanares'],
            ['nombre' => 'Manizales', 'slug' => 'manizales'],
            ['nombre' => 'La Dorada', 'slug' => 'la-dorada'],
            ['nombre' => 'Filadelfia', 'slug' => 'filadelfia'],
            ['nombre' => 'Aranzazu', 'slug' => 'aranzazu'],
            ['nombre' => 'Aguadas', 'slug' => 'aguadas'],
            ['nombre' => 'La Merced', 'slug' => 'la-merced'],
            ['nombre' => 'San José', 'slug' => 'san-jose'],
            ['nombre' => 'Cartagena', 'slug' => 'cartagena'],
            ['nombre' => 'Marulanda', 'slug' => 'marulanda'],
            ['nombre' => 'Belalcázar', 'slug' => 'belalcazar'],
            ['nombre' => 'Palestina', 'slug' => 'palestina'],
            ['nombre' => 'Supía', 'slug' => 'supia'],
            ['nombre' => 'Viterbo', 'slug' => 'viterbo'],
            ['nombre' => 'Salamina', 'slug' => 'salamina'],
            ['nombre' => 'Riosucio', 'slug' => 'riosucio'],
            ['nombre' => 'Anserma', 'slug' => 'anserma'],
            ['nombre' => 'Chinchiná', 'slug' => 'chinchina'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'caqueta'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'San Vicente del Caguán', 'slug' => 'san-vicente-del-caguan'],
            ['nombre' => 'Morelia', 'slug' => 'morelia'],
            ['nombre' => 'Milán', 'slug' => 'milan'],
            ['nombre' => 'La Montañita', 'slug' => 'la-montanita'],
            ['nombre' => 'Florencia', 'slug' => 'florencia'],
            ['nombre' => 'Solita', 'slug' => 'solita'],
            ['nombre' => 'Belén de Andaquies', 'slug' => 'belen-de-andaquies'],
            ['nombre' => 'Cartagena de Chairá', 'slug' => 'cartagena-de-chaira'],
            ['nombre' => 'San José de la Fragua', 'slug' => 'san-jose-de-la-fragua'],
            ['nombre' => 'Solano', 'slug' => 'solano'],
            ['nombre' => 'Valparaíso', 'slug' => 'valparaiso'],
            ['nombre' => 'Albania', 'slug' => 'albania'],
            ['nombre' => 'El Paujil', 'slug' => 'el-paujil'],
            ['nombre' => 'Curillo', 'slug' => 'curillo'],
            ['nombre' => 'Puerto Rico', 'slug' => 'puerto-rico'],
            ['nombre' => 'El Doncello', 'slug' => 'el-doncello'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'casanare'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Yopal', 'slug' => 'yopal'],
            ['nombre' => 'Trinidad', 'slug' => 'trinidad'],
            ['nombre' => 'Pore', 'slug' => 'pore'],
            ['nombre' => 'Paz de Ariporo', 'slug' => 'paz-de-ariporo'],
            ['nombre' => 'Orocué', 'slug' => 'orocue'],
            ['nombre' => 'Aguazul', 'slug' => 'aguazul'],
            ['nombre' => 'Monterrey', 'slug' => 'monterrey'],
            ['nombre' => 'Villanueva', 'slug' => 'villanueva'],
            ['nombre' => 'Hato Corozal', 'slug' => 'hato-corozal'],
            ['nombre' => 'La Salina', 'slug' => 'la-salina'],
            ['nombre' => 'Recetor', 'slug' => 'recetor'],
            ['nombre' => 'Sácama', 'slug' => 'sacama'],
            ['nombre' => 'Chámeza', 'slug' => 'chameza'],
            ['nombre' => 'Nunchía', 'slug' => 'nunchia'],
            ['nombre' => 'Sabanalarga', 'slug' => 'sabanalarga'],
            ['nombre' => 'Tamara', 'slug' => 'tamara'],
            ['nombre' => 'San Luis de Palenque', 'slug' => 'san-luis-de-palenque'],
            ['nombre' => 'Tauramena', 'slug' => 'tauramena'],
            ['nombre' => 'Maní', 'slug' => 'mani'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'cauca'])->firstOrFail();

        $ciudades = [];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'cesar'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Valledupar', 'slug' => 'valledupar'],
            ['nombre' => 'San Diego', 'slug' => 'san-diego'],
            ['nombre' => 'Río de Oro', 'slug' => 'rio-de-oro'],
            ['nombre' => 'La Gloria', 'slug' => 'la-gloria'],
            ['nombre' => 'González', 'slug' => 'gonzalez'],
            ['nombre' => 'Gamarra', 'slug' => 'gamarra'],
            ['nombre' => 'El Paso', 'slug' => 'el-paso'],
            ['nombre' => 'San Alberto', 'slug' => 'san-alberto'],
            ['nombre' => 'Bosconia', 'slug' => 'bosconia'],
            ['nombre' => 'Codazzi', 'slug' => 'codazzi'],
            ['nombre' => 'Manaure', 'slug' => 'manaure'],
            ['nombre' => 'Pueblo Bello', 'slug' => 'pueblo-bello'],
            ['nombre' => 'Robles La Paz', 'slug' => 'robles-la-paz'],
            ['nombre' => 'Tamalameque', 'slug' => 'tamalameque'],
            ['nombre' => 'San Martín', 'slug' => 'san-martin'],
            ['nombre' => 'Astrea', 'slug' => 'astrea'],
            ['nombre' => 'Becerril', 'slug' => 'becerril'],
            ['nombre' => 'Pailitas', 'slug' => 'pailitas'],
            ['nombre' => 'Pelaya', 'slug' => 'pelaya'],
            ['nombre' => 'Chiriguaná', 'slug' => 'chiriguana'],
            ['nombre' => 'Chimichagua', 'slug' => 'chimichagua'],
            ['nombre' => 'El Copey', 'slug' => 'el-copey'],
            ['nombre' => 'La Jagua de Ibirico', 'slug' => 'la-jagua-de-ibirico'],
            ['nombre' => 'Curumaní', 'slug' => 'curumani'],
            ['nombre' => 'Aguachica', 'slug' => 'aguachica'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'choco'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Juradó', 'slug' => 'jurado'],
            ['nombre' => 'Bellavista', 'slug' => 'bellavista'],
            ['nombre' => 'Carmen del darien', 'slug' => 'carmen-del-darien'],
            ['nombre' => 'Medio atrato', 'slug' => 'medio-atrato'],
            ['nombre' => 'Rio quito', 'slug' => 'rio-quito'],
            ['nombre' => 'Unión Panombrericana', 'slug' => 'union-panombrericana'],
            ['nombre' => 'Atrato', 'slug' => 'atrato'],
            ['nombre' => 'Belén de Bajirá', 'slug' => 'belen-de-bajira'],
            ['nombre' => 'El Carmen de Atrato', 'slug' => 'el-carmen-de-atrato'],
            ['nombre' => 'Litoral del Sanjuán', 'slug' => 'litoral-del-sanjuan'],
            ['nombre' => 'Medio Baudó', 'slug' => 'medio-baudo'],
            ['nombre' => 'Medio Sanjuán', 'slug' => 'medio-sanjuan'],
            ['nombre' => 'Río Iró', 'slug' => 'rio-iro'],
            ['nombre' => 'Sipí', 'slug' => 'sipi'],
            ['nombre' => 'Nóvita', 'slug' => 'novita'],
            ['nombre' => 'San José del Palmar', 'slug' => 'san-jose-del-palmar'],
            ['nombre' => 'Lloró', 'slug' => 'lloro'],
            ['nombre' => 'Nuquí', 'slug' => 'nuqui'],
            ['nombre' => 'Cértegui', 'slug' => 'certegui'],
            ['nombre' => 'Mutis', 'slug' => 'mutis'],
            ['nombre' => 'Pié de Pató', 'slug' => 'pie-de-pato'],
            ['nombre' => 'El Cantón de San Pablo', 'slug' => 'el-canton-de-san-pablo'],
            ['nombre' => 'Unguía', 'slug' => 'unguia'],
            ['nombre' => 'Bagadó', 'slug' => 'bagado'],
            ['nombre' => 'Acandí', 'slug' => 'acandi'],
            ['nombre' => 'Pizarro', 'slug' => 'pizarro'],
            ['nombre' => 'Riosucio', 'slug' => 'riosucio'],
            ['nombre' => 'Condoto', 'slug' => 'condoto'],
            ['nombre' => 'Tadó', 'slug' => 'tado'],
            ['nombre' => 'Istmina', 'slug' => 'istmina'],
            ['nombre' => 'Quibdó', 'slug' => 'quibdo'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'cordoba'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Valencia', 'slug' => 'valencia'],
            ['nombre' => 'San Andres de Sotavento', 'slug' => 'san-andres-de-sotavento'],
            ['nombre' => 'Puerto Escondido', 'slug' => 'puerto-escondido'],
            ['nombre' => 'Pueblo Nuevo', 'slug' => 'pueblo-nuevo'],
            ['nombre' => 'Lorica', 'slug' => 'lorica'],
            ['nombre' => 'Cotoca Arriba', 'slug' => 'cotoca-arriba'],
            ['nombre' => 'Apartadade', 'slug' => 'apartadade'],
            ['nombre' => 'Buenavista', 'slug' => 'buenavista'],
            ['nombre' => 'Puerto Libertador', 'slug' => 'puerto-libertador'],
            ['nombre' => 'San Andres Island', 'slug' => 'san-andres-island'],
            ['nombre' => 'Los Córdobas', 'slug' => 'los-cordobas'],
            ['nombre' => 'Canalete', 'slug' => 'canalete'],
            ['nombre' => 'Chimá', 'slug' => 'chima'],
            ['nombre' => 'San Carlos', 'slug' => 'san-carlos'],
            ['nombre' => 'Moñitos', 'slug' => 'monitos'],
            ['nombre' => 'San Pelayo', 'slug' => 'san-pelayo'],
            ['nombre' => 'Momil', 'slug' => 'momil'],
            ['nombre' => 'Purísima', 'slug' => 'purisima'],
            ['nombre' => 'San Bernardo del Viento', 'slug' => 'san-bernardo-del-viento'],
            ['nombre' => 'San Antero', 'slug' => 'san-antero'],
            ['nombre' => 'Ciénaga de Oro', 'slug' => 'cienaga-de-oro'],
            ['nombre' => 'Chinú', 'slug' => 'chinu'],
            ['nombre' => 'Ayapel', 'slug' => 'ayapel'],
            ['nombre' => 'Tierralta', 'slug' => 'tierralta'],
            ['nombre' => 'Planeta Rica', 'slug' => 'planeta-rica'],
            ['nombre' => 'Cereté', 'slug' => 'cerete'],
            ['nombre' => 'Sahagún', 'slug' => 'sahagun'],
            ['nombre' => 'Montelíbano', 'slug' => 'montelibano'],
            ['nombre' => 'Montería', 'slug' => 'monteria'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'cundinamarca'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Yacopí', 'slug' => 'yacopi'],
            ['nombre' => 'Villeta', 'slug' => 'villeta'],
            ['nombre' => 'Vergara', 'slug' => 'vergara'],
            ['nombre' => 'Tenjo', 'slug' => 'tenjo'],
            ['nombre' => 'San Cayetano', 'slug' => 'san-cayetano'],
            ['nombre' => 'San Bernardo', 'slug' => 'san-bernardo'],
            ['nombre' => 'Quebradanegra', 'slug' => 'quebradanegra'],
            ['nombre' => 'Manta', 'slug' => 'manta'],
            ['nombre' => 'Madrid', 'slug' => 'madrid'],
            ['nombre' => 'La Vega', 'slug' => 'la-vega'],
            ['nombre' => 'La Palma', 'slug' => 'la-palma'],
            ['nombre' => 'La Mesa', 'slug' => 'la-mesa'],
            ['nombre' => 'La Calera', 'slug' => 'la-calera'],
            ['nombre' => 'Junín', 'slug' => 'junin'],
            ['nombre' => 'Jerusalén', 'slug' => 'jerusalen'],
            ['nombre' => 'Gutiérrez', 'slug' => 'gutierrez'],
            ['nombre' => 'Guaduas', 'slug' => 'guaduas'],
            ['nombre' => 'Girardot', 'slug' => 'girardot'],
            ['nombre' => 'Fúquene', 'slug' => 'fuquene'],
            ['nombre' => 'El Peñón', 'slug' => 'el-penon'],
            ['nombre' => 'El Colegio', 'slug' => 'el-colegio'],
            ['nombre' => 'Chía', 'slug' => 'chia'],
            ['nombre' => 'Cachipay', 'slug' => 'cachipay'],
            ['nombre' => 'Beltrán', 'slug' => 'beltran'],
            ['nombre' => 'Albán', 'slug' => 'alban'],
            ['nombre' => 'Agua de Dios', 'slug' => 'agua-de-dios'],
            ['nombre' => 'Gachetá', 'slug' => 'gacheta'],
            ['nombre' => 'La Peña', 'slug' => 'la-pena'],
            ['nombre' => 'Salitre Alto', 'slug' => 'salitre-alto'],
            ['nombre' => 'San Antonio Del Tequendam', 'slug' => 'san-antonio-del-tequendam'],
            ['nombre' => 'San Juan De Rio Seco', 'slug' => 'san-juan-de-rio-seco'],
            ['nombre' => 'Bituima', 'slug' => 'bituima'],
            ['nombre' => 'Gama', 'slug' => 'gama'],
            ['nombre' => 'Pulí', 'slug' => 'puli'],
            ['nombre' => 'Nimaima', 'slug' => 'nimaima'],
            ['nombre' => 'Tena', 'slug' => 'tena'],
            ['nombre' => 'Villagómez', 'slug' => 'villagomez'],
            ['nombre' => 'Paime', 'slug' => 'paime'],
            ['nombre' => 'Tausa', 'slug' => 'tausa'],
            ['nombre' => 'Tibirita', 'slug' => 'tibirita'],
            ['nombre' => 'Ubaque', 'slug' => 'ubaque'],
            ['nombre' => 'Tibacuy', 'slug' => 'tibacuy'],
            ['nombre' => 'Guayabal de Síquima', 'slug' => 'guayabal-de-siquima'],
            ['nombre' => 'Topaipí', 'slug' => 'topaipi'],
            ['nombre' => 'Chaguaní', 'slug' => 'chaguani'],
            ['nombre' => 'Nilo', 'slug' => 'nilo'],
            ['nombre' => 'Guataquí', 'slug' => 'guataqui'],
            ['nombre' => 'Nariño', 'slug' => 'narino'],
            ['nombre' => 'Venecia', 'slug' => 'venecia'],
            ['nombre' => 'Sutatausa', 'slug' => 'sutatausa'],
            ['nombre' => 'Pandi', 'slug' => 'pandi'],
            ['nombre' => 'Quetame', 'slug' => 'quetame'],
            ['nombre' => 'Cabrera', 'slug' => 'cabrera'],
            ['nombre' => 'Fosca', 'slug' => 'fosca'],
            ['nombre' => 'Zipacón', 'slug' => 'zipacon'],
            ['nombre' => 'Vianí', 'slug' => 'viani'],
            ['nombre' => 'Susa', 'slug' => 'susa'],
            ['nombre' => 'Gachalá', 'slug' => 'gachala'],
            ['nombre' => 'Cucunubá', 'slug' => 'cucunuba'],
            ['nombre' => 'Machetá', 'slug' => 'macheta'],
            ['nombre' => 'Sesquilé', 'slug' => 'sesquile'],
            ['nombre' => 'Ubalá', 'slug' => 'ubala'],
            ['nombre' => 'Quipile', 'slug' => 'quipile'],
            ['nombre' => 'Supatá', 'slug' => 'supata'],
            ['nombre' => 'Guatavita', 'slug' => 'guatavita'],
            ['nombre' => 'Carmen de Carupa', 'slug' => 'carmen-de-carupa'],
            ['nombre' => 'Guayabetal', 'slug' => 'guayabetal'],
            ['nombre' => 'Paratebueno', 'slug' => 'paratebueno'],
            ['nombre' => 'Nocaima', 'slug' => 'nocaima'],
            ['nombre' => 'Lenguazaque', 'slug' => 'lenguazaque'],
            ['nombre' => 'Ricaurte', 'slug' => 'ricaurte'],
            ['nombre' => 'Chipaque', 'slug' => 'chipaque'],
            ['nombre' => 'San Francisco', 'slug' => 'san-francisco'],
            ['nombre' => 'Utica', 'slug' => 'utica'],
            ['nombre' => 'Sasaima', 'slug' => 'sasaima'],
            ['nombre' => 'Medina', 'slug' => 'medina'],
            ['nombre' => 'Gachancipá', 'slug' => 'gachancipa'],
            ['nombre' => 'Pasca', 'slug' => 'pasca'],
            ['nombre' => 'Une', 'slug' => 'une'],
            ['nombre' => 'Guasca', 'slug' => 'guasca'],
            ['nombre' => 'Apulo', 'slug' => 'apulo'],
            ['nombre' => 'Subachoque', 'slug' => 'subachoque'],
            ['nombre' => 'Tabio', 'slug' => 'tabio'],
            ['nombre' => 'Guachetá', 'slug' => 'guacheta'],
            ['nombre' => 'Choachí', 'slug' => 'choachi'],
            ['nombre' => 'Bojacá', 'slug' => 'bojaca'],
            ['nombre' => 'Caparrapí', 'slug' => 'caparrapi'],
            ['nombre' => 'Cogua', 'slug' => 'cogua'],
            ['nombre' => 'Simijaca', 'slug' => 'simijaca'],
            ['nombre' => 'Suesca', 'slug' => 'suesca'],
            ['nombre' => 'Anapoima', 'slug' => 'anapoima'],
            ['nombre' => 'Arbeláez', 'slug' => 'arbelaez'],
            ['nombre' => 'Fómeque', 'slug' => 'fomeque'],
            ['nombre' => 'Nemocón', 'slug' => 'nemocon'],
            ['nombre' => 'Anolaima', 'slug' => 'anolaima'],
            ['nombre' => 'Viotá', 'slug' => 'viota'],
            ['nombre' => 'Villapinzón', 'slug' => 'villapinzon'],
            ['nombre' => 'Tocancipá', 'slug' => 'tocancipa'],
            ['nombre' => 'Chocontá', 'slug' => 'choconta'],
            ['nombre' => 'Cota', 'slug' => 'cota'],
            ['nombre' => 'Cáqueza', 'slug' => 'caqueza'],
            ['nombre' => 'Silvania', 'slug' => 'silvania'],
            ['nombre' => 'Sopó', 'slug' => 'sopo'],
            ['nombre' => 'Puerto Salgar', 'slug' => 'puerto-salgar'],
            ['nombre' => 'Tocaima', 'slug' => 'tocaima'],
            ['nombre' => 'Pacho', 'slug' => 'pacho'],
            ['nombre' => 'Ubaté', 'slug' => 'ubate'],
            ['nombre' => 'Sibaté', 'slug' => 'sibate'],
            ['nombre' => 'Cajicá', 'slug' => 'cajica'],
            ['nombre' => 'Mosquera', 'slug' => 'mosquera'],
            ['nombre' => 'Funza', 'slug' => 'funza'],
            ['nombre' => 'Fusagasuga', 'slug' => 'fusagasuga'],
            ['nombre' => 'Zipaquirá', 'slug' => 'zipaquira'],
            ['nombre' => 'Facatativá', 'slug' => 'facatativa'],
            ['nombre' => 'Soacha', 'slug' => 'soacha'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'distrito-capital-de-bogota'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Bogotá', 'slug' => 'bogota'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'guainia'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'San Felipe', 'slug' => 'san-felipe'],
            ['nombre' => 'La Guadalupe', 'slug' => 'la-guadalupe'],
            ['nombre' => 'Puerto Colombia', 'slug' => 'puerto-colombia'],
            ['nombre' => 'Barranco Minas', 'slug' => 'barranco-minas'],
            ['nombre' => 'Pana Pana', 'slug' => 'pana-pana'],
            ['nombre' => 'Cacahual', 'slug' => 'cacahual'],
            ['nombre' => 'Inírida', 'slug' => 'inirida'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'guaviare'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'El Retorno', 'slug' => 'el-retorno'],
            ['nombre' => 'Calamar', 'slug' => 'calamar'],
            ['nombre' => 'Miraflores', 'slug' => 'miraflores'],
            ['nombre' => 'San José del Guaviare', 'slug' => 'san-jose-del-guaviare'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'huila'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Yaguará', 'slug' => 'yaguara'],
            ['nombre' => 'Tarqui', 'slug' => 'tarqui'],
            ['nombre' => 'Rivera', 'slug' => 'rivera'],
            ['nombre' => 'Pitalito', 'slug' => 'pitalito'],
            ['nombre' => 'Pital', 'slug' => 'pital'],
            ['nombre' => 'Palermo', 'slug' => 'palermo'],
            ['nombre' => 'Neiva', 'slug' => 'neiva'],
            ['nombre' => 'La Plata', 'slug' => 'la-plata'],
            ['nombre' => 'La Argentina', 'slug' => 'la-argentina'],
            ['nombre' => 'Colombia', 'slug' => 'colombia'],
            ['nombre' => 'Baraya', 'slug' => 'baraya'],
            ['nombre' => 'Altamira', 'slug' => 'altamira'],
            ['nombre' => 'Guadalupe', 'slug' => 'guadalupe'],
            ['nombre' => 'San Agustín', 'slug' => 'san-agustin'],
            ['nombre' => 'Santa María', 'slug' => 'santa-maria'],
            ['nombre' => 'Palestina', 'slug' => 'palestina'],
            ['nombre' => 'Campo Alegre', 'slug' => 'campo-alegre'],
            ['nombre' => 'Salado Blanco', 'slug' => 'salado-blanco'],
            ['nombre' => 'Elías', 'slug' => 'elias'],
            ['nombre' => 'Paicol', 'slug' => 'paicol'],
            ['nombre' => 'Nátaga', 'slug' => 'nataga'],
            ['nombre' => 'Oporapa', 'slug' => 'oporapa'],
            ['nombre' => 'Suaza', 'slug' => 'suaza'],
            ['nombre' => 'Villavieja', 'slug' => 'villavieja'],
            ['nombre' => 'Teruel', 'slug' => 'teruel'],
            ['nombre' => 'Íquira', 'slug' => 'iquira'],
            ['nombre' => 'Tesalia', 'slug' => 'tesalia'],
            ['nombre' => 'Hobo', 'slug' => 'hobo'],
            ['nombre' => 'Acevedo', 'slug' => 'acevedo'],
            ['nombre' => 'Agrado', 'slug' => 'agrado'],
            ['nombre' => 'Tello', 'slug' => 'tello'],
            ['nombre' => 'Isnos', 'slug' => 'isnos'],
            ['nombre' => 'Aipe', 'slug' => 'aipe'],
            ['nombre' => 'Timaná', 'slug' => 'timana'],
            ['nombre' => 'Gigante', 'slug' => 'gigante'],
            ['nombre' => 'Algeciras', 'slug' => 'algeciras'],
            ['nombre' => 'Garzón', 'slug' => 'garzon'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'la-guajira'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Villanueva', 'slug' => 'villanueva'],
            ['nombre' => 'Uribia', 'slug' => 'uribia'],
            ['nombre' => 'Hatonuevo', 'slug' => 'hatonuevo'],
            ['nombre' => 'El Molino', 'slug' => 'el-molino'],
            ['nombre' => 'Calabacito', 'slug' => 'calabacito'],
            ['nombre' => 'Barrancas', 'slug' => 'barrancas'],
            ['nombre' => 'La Jagua del Pilar', 'slug' => 'la-jagua-del-pilar'],
            ['nombre' => 'Dibulla', 'slug' => 'dibulla'],
            ['nombre' => 'Distracción', 'slug' => 'distraccion'],
            ['nombre' => 'Urumita', 'slug' => 'urumita'],
            ['nombre' => 'Manaure', 'slug' => 'manaure'],
            ['nombre' => 'Fonseca', 'slug' => 'fonseca'],
            ['nombre' => 'San Juan del Cesar', 'slug' => 'san-juan-del-cesar'],
            ['nombre' => 'Ríohacha', 'slug' => 'riohacha'],
            ['nombre' => 'Maicao', 'slug' => 'maicao'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'magdalena'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Santa Marta', 'slug' => 'santa-marta'],
            ['nombre' => 'Santa Ana', 'slug' => 'santa-ana'],
            ['nombre' => 'Salamina', 'slug' => 'salamina'],
            ['nombre' => 'Remolino', 'slug' => 'remolino'],
            ['nombre' => 'Pueblo Bello', 'slug' => 'pueblo-bello'],
            ['nombre' => 'Minca', 'slug' => 'minca'],
            ['nombre' => 'Fundación', 'slug' => 'fundacion'],
            ['nombre' => 'El Piñón', 'slug' => 'el-pinon'],
            ['nombre' => 'El Difícil', 'slug' => 'el-dificil'],
            ['nombre' => 'El Banco', 'slug' => 'el-banco'],
            ['nombre' => 'Ciénaga', 'slug' => 'cienaga'],
            ['nombre' => 'Barranquilla', 'slug' => 'barranquilla'],
            ['nombre' => 'San Sebastian De Buenavis', 'slug' => 'san-sebastian-de-buenavis'],
            ['nombre' => 'Pedraza', 'slug' => 'pedraza'],
            ['nombre' => 'Buenavista', 'slug' => 'buenavista'],
            ['nombre' => 'Cerro de San Antonio', 'slug' => 'cerro-de-san-antonio'],
            ['nombre' => 'San Antonio', 'slug' => 'san-antonio'],
            ['nombre' => 'Guamal', 'slug' => 'guamal'],
            ['nombre' => 'Chivolo', 'slug' => 'chivolo'],
            ['nombre' => 'Aracataca', 'slug' => 'aracataca'],
            ['nombre' => 'Pivijay', 'slug' => 'pivijay'],
            ['nombre' => 'Plato', 'slug' => 'plato'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'meta'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Villavicencio', 'slug' => 'villavicencio'],
            ['nombre' => 'San Juanito', 'slug' => 'san-juanito'],
            ['nombre' => 'Puerto López', 'slug' => 'puerto-lopez'],
            ['nombre' => 'Puerto Lleras', 'slug' => 'puerto-lleras'],
            ['nombre' => 'El Castillo', 'slug' => 'el-castillo'],
            ['nombre' => 'El Calvario', 'slug' => 'el-calvario'],
            ['nombre' => 'Mesetas', 'slug' => 'mesetas'],
            ['nombre' => 'El Dorado', 'slug' => 'el-dorado'],
            ['nombre' => 'La Macarena', 'slug' => 'la-macarena'],
            ['nombre' => 'Puerto Gaitán', 'slug' => 'puerto-gaitan'],
            ['nombre' => 'La Uribe', 'slug' => 'la-uribe'],
            ['nombre' => 'Vistahermosa', 'slug' => 'vistahermosa'],
            ['nombre' => 'Cabuyaro', 'slug' => 'cabuyaro'],
            ['nombre' => 'Barranca de Upía', 'slug' => 'barranca-de-upia'],
            ['nombre' => 'Castilla La Nueva', 'slug' => 'castilla-la-nueva'],
            ['nombre' => 'San Carlos de Guaroa', 'slug' => 'san-carlos-de-guaroa'],
            ['nombre' => 'Mapiripán', 'slug' => 'mapiripan'],
            ['nombre' => 'Cubarral', 'slug' => 'cubarral'],
            ['nombre' => 'San Juan de Arama', 'slug' => 'san-juan-de-arama'],
            ['nombre' => 'Fuente de Oro', 'slug' => 'fuente-de-oro'],
            ['nombre' => 'Puerto Concordia', 'slug' => 'puerto-concordia'],
            ['nombre' => 'Guamal', 'slug' => 'guamal'],
            ['nombre' => 'Puerto Rico', 'slug' => 'puerto-rico'],
            ['nombre' => 'Lejanías', 'slug' => 'lejanias'],
            ['nombre' => 'Restrepo', 'slug' => 'restrepo'],
            ['nombre' => 'San Martín', 'slug' => 'san-martin'],
            ['nombre' => 'Granada', 'slug' => 'granada'],
            ['nombre' => 'Acacías', 'slug' => 'acacias'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'narino'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Santa Rosa', 'slug' => 'santa-rosa'],
            ['nombre' => 'Santa Cruz de Robles', 'slug' => 'santa-cruz-de-robles'],
            ['nombre' => 'San José', 'slug' => 'san-jose'],
            ['nombre' => 'Ricaurte', 'slug' => 'ricaurte'],
            ['nombre' => 'Puerres', 'slug' => 'puerres'],
            ['nombre' => 'Potosí', 'slug' => 'potosi'],
            ['nombre' => 'Payán', 'slug' => 'payan'],
            ['nombre' => 'La Llanada', 'slug' => 'la-llanada'],
            ['nombre' => 'La Florida', 'slug' => 'la-florida'],
            ['nombre' => 'Espriella', 'slug' => 'espriella'],
            ['nombre' => 'La Cruz', 'slug' => 'la-cruz'],
            ['nombre' => 'El Rosario', 'slug' => 'el-rosario'],
            ['nombre' => 'El Charco', 'slug' => 'el-charco'],
            ['nombre' => 'El Carmen', 'slug' => 'el-carmen'],
            ['nombre' => 'Cumbitara', 'slug' => 'cumbitara'],
            ['nombre' => 'Cartago', 'slug' => 'cartago'],
            ['nombre' => 'Providencia', 'slug' => 'providencia'],
            ['nombre' => 'Bocas de Satinga', 'slug' => 'bocas-de-satinga'],
            ['nombre' => 'Belén', 'slug' => 'belen'],
            ['nombre' => 'El Tablón', 'slug' => 'el-tablon'],
            ['nombre' => 'San Lorenzo', 'slug' => 'san-lorenzo'],
            ['nombre' => 'San Pablo', 'slug' => 'san-pablo'],
            ['nombre' => 'Sapúyes', 'slug' => 'sapuyes'],
            ['nombre' => 'La Tola', 'slug' => 'la-tola'],
            ['nombre' => 'La Unión', 'slug' => 'la-union'],
            ['nombre' => 'Panán', 'slug' => 'panan'],
            ['nombre' => 'Genova', 'slug' => 'genova'],
            ['nombre' => 'Contadero', 'slug' => 'contadero'],
            ['nombre' => 'Imués', 'slug' => 'imues'],
            ['nombre' => 'Berruecos', 'slug' => 'berruecos'],
            ['nombre' => 'Mallama', 'slug' => 'mallama'],
            ['nombre' => 'Iles', 'slug' => 'iles'],
            ['nombre' => 'Aldana', 'slug' => 'aldana'],
            ['nombre' => 'Consacá', 'slug' => 'consaca'],
            ['nombre' => 'Policarpa', 'slug' => 'policarpa'],
            ['nombre' => 'Funes', 'slug' => 'funes'],
            ['nombre' => 'Gualmatán', 'slug' => 'gualmatan'],
            ['nombre' => 'Yacuanquer', 'slug' => 'yacuanquer'],
            ['nombre' => 'Ospina', 'slug' => 'ospina'],
            ['nombre' => 'Taminango', 'slug' => 'taminango'],
            ['nombre' => 'Tangua', 'slug' => 'tangua'],
            ['nombre' => 'Mosquera', 'slug' => 'mosquera'],
            ['nombre' => 'Linares', 'slug' => 'linares'],
            ['nombre' => 'Leiva', 'slug' => 'leiva'],
            ['nombre' => 'Guachucal', 'slug' => 'guachucal'],
            ['nombre' => 'Buesaco', 'slug' => 'buesaco'],
            ['nombre' => 'Sotomayor', 'slug' => 'sotomayor'],
            ['nombre' => 'Iscuandé', 'slug' => 'iscuande'],
            ['nombre' => 'Chachagüí', 'slug' => 'chachagui'],
            ['nombre' => 'Ancuya', 'slug' => 'ancuya'],
            ['nombre' => 'Francisco Pizarro', 'slug' => 'francisco-pizarro'],
            ['nombre' => 'Guaitarilla', 'slug' => 'guaitarilla'],
            ['nombre' => 'Pupiales', 'slug' => 'pupiales'],
            ['nombre' => 'Cumbal', 'slug' => 'cumbal'],
            ['nombre' => 'Barbacoas', 'slug' => 'barbacoas'],
            ['nombre' => 'Sandoná', 'slug' => 'sandona'],
            ['nombre' => 'Samaniego', 'slug' => 'samaniego'],
            ['nombre' => 'Túquerres', 'slug' => 'tuquerres'],
            ['nombre' => 'Ipiales', 'slug' => 'ipiales'],
            ['nombre' => 'Tumaco', 'slug' => 'tumaco'],
            ['nombre' => 'Pasto', 'slug' => 'pasto'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'norte-de-santander'])->firstOrFail();

        $ciudades = [];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'provid_departmentencia-y-santa-catalina-archipielago-de-san-andres'])->firstOrFail();

        $ciudades = [];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'putumayo'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Villagarzón', 'slug' => 'villagarzon'],
            ['nombre' => 'Santiago', 'slug' => 'santiago'],
            ['nombre' => 'Mocoa', 'slug' => 'mocoa'],
            ['nombre' => 'Colón', 'slug' => 'colon'],
            ['nombre' => 'Puerto Guzmán', 'slug' => 'puerto-guzman'],
            ['nombre' => 'Puerto Caicedo', 'slug' => 'puerto-caicedo'],
            ['nombre' => 'San Francisco', 'slug' => 'san-francisco'],
            ['nombre' => 'Puerto Leguízamo', 'slug' => 'puerto-leguizamo'],
            ['nombre' => 'Sibundoy', 'slug' => 'sibundoy'],
            ['nombre' => 'Valle del Guamuez', 'slug' => 'valle-del-guamuez'],
            ['nombre' => 'Orito', 'slug' => 'orito'],
            ['nombre' => 'Puerto Asís', 'slug' => 'puerto-asis'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'quindio'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Salento', 'slug' => 'salento'],
            ['nombre' => 'Quimbaya', 'slug' => 'quimbaya'],
            ['nombre' => 'Montenegro', 'slug' => 'montenegro'],
            ['nombre' => 'La Tebaida', 'slug' => 'la-tebaida'],
            ['nombre' => 'Filandia', 'slug' => 'filandia'],
            ['nombre' => 'Circasia', 'slug' => 'circasia'],
            ['nombre' => 'Calarcá', 'slug' => 'calarca'],
            ['nombre' => 'Génova', 'slug' => 'genova'],
            ['nombre' => 'Buenavista', 'slug' => 'buenavista'],
            ['nombre' => 'Córdoba', 'slug' => 'cordoba'],
            ['nombre' => 'Pijao', 'slug' => 'pijao'],
            ['nombre' => 'Armenia', 'slug' => 'armenia'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'risaralda'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Pueblo Rico', 'slug' => 'pueblo-rico'],
            ['nombre' => 'Marsella', 'slug' => 'marsella'],
            ['nombre' => 'La Virginia', 'slug' => 'la-virginia'],
            ['nombre' => 'Dos Quebradas', 'slug' => 'dos-quebradas'],
            ['nombre' => 'Balboa', 'slug' => 'balboa'],
            ['nombre' => 'Eje Cafetero', 'slug' => 'eje-cafetero'],
            ['nombre' => 'Guática', 'slug' => 'guatica'],
            ['nombre' => 'La Celia', 'slug' => 'la-celia'],
            ['nombre' => 'Mistrató', 'slug' => 'mistrato'],
            ['nombre' => 'Santuario', 'slug' => 'santuario'],
            ['nombre' => 'Apía', 'slug' => 'apia'],
            ['nombre' => 'Quinchía', 'slug' => 'quinchia'],
            ['nombre' => 'Belén de Umbría', 'slug' => 'belen-de-umbria'],
            ['nombre' => 'Santa Rosa de Cabal', 'slug' => 'santa-rosa-de-cabal'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'santander'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Vetas', 'slug' => 'vetas'],
            ['nombre' => 'Vélez', 'slug' => 'velez'],
            ['nombre' => 'Tona', 'slug' => 'tona'],
            ['nombre' => 'Socorro', 'slug' => 'socorro'],
            ['nombre' => 'Puerto Wilches', 'slug' => 'puerto-wilches'],
            ['nombre' => 'Piedecuesta', 'slug' => 'piedecuesta'],
            ['nombre' => 'Páramo', 'slug' => 'paramo'],
            ['nombre' => 'Palmas del Socorro', 'slug' => 'palmas-del-socorro'],
            ['nombre' => 'Palmar', 'slug' => 'palmar'],
            ['nombre' => 'Málaga', 'slug' => 'malaga'],
            ['nombre' => 'La Paz', 'slug' => 'la-paz'],
            ['nombre' => 'Jordán', 'slug' => 'jordan'],
            ['nombre' => 'Gámbita', 'slug' => 'gambita'],
            ['nombre' => 'Floridablanca', 'slug' => 'floridablanca'],
            ['nombre' => 'Florián', 'slug' => 'florian'],
            ['nombre' => 'Enciso', 'slug' => 'enciso'],
            ['nombre' => 'El Playón', 'slug' => 'el-playon'],
            ['nombre' => 'El Hato', 'slug' => 'el-hato'],
            ['nombre' => 'El Guacamayo', 'slug' => 'el-guacamayo'],
            ['nombre' => 'Curití', 'slug' => 'curiti'],
            ['nombre' => 'Confines', 'slug' => 'confines'],
            ['nombre' => 'Cimitarra', 'slug' => 'cimitarra'],
            ['nombre' => 'Charalá', 'slug' => 'charala'],
            ['nombre' => 'Cerrito', 'slug' => 'cerrito'],
            ['nombre' => 'California', 'slug' => 'california'],
            ['nombre' => 'Bucaramanga', 'slug' => 'bucaramanga'],
            ['nombre' => 'Galan', 'slug' => 'galan'],
            ['nombre' => 'San Joaquín', 'slug' => 'san-joaquin'],
            ['nombre' => 'Guapota', 'slug' => 'guapota'],
            ['nombre' => 'El Peñón', 'slug' => 'el-penon'],
            ['nombre' => 'La Belleza', 'slug' => 'la-belleza'],
            ['nombre' => 'Santa Bárbara', 'slug' => 'santa-barbara'],
            ['nombre' => 'San Andrés', 'slug' => 'san-andres'],
            ['nombre' => 'Rionegro', 'slug' => 'rionegro'],
            ['nombre' => 'San Benito', 'slug' => 'san-benito'],
            ['nombre' => 'Aguada', 'slug' => 'aguada'],
            ['nombre' => 'San Jose Miranda', 'slug' => 'san-jose-miranda'],
            ['nombre' => 'Santa Helena De Opon', 'slug' => 'santa-helena-de-opon'],
            ['nombre' => 'Cabrera', 'slug' => 'cabrera'],
            ['nombre' => 'Cepitá', 'slug' => 'cepita'],
            ['nombre' => 'Encino', 'slug' => 'encino'],
            ['nombre' => 'Macaravita', 'slug' => 'macaravita'],
            ['nombre' => 'San Miguel', 'slug' => 'san-miguel'],
            ['nombre' => 'Charta', 'slug' => 'charta'],
            ['nombre' => 'Pinchote', 'slug' => 'pinchote'],
            ['nombre' => 'Chima', 'slug' => 'chima'],
            ['nombre' => 'Suratá', 'slug' => 'surata'],
            ['nombre' => 'Albania', 'slug' => 'albania'],
            ['nombre' => 'Jesús María', 'slug' => 'jesus-maria'],
            ['nombre' => 'Carcasí', 'slug' => 'carcasi'],
            ['nombre' => 'Chipatá', 'slug' => 'chipata'],
            ['nombre' => 'Ocamonte', 'slug' => 'ocamonte'],
            ['nombre' => 'Guavatá', 'slug' => 'guavata'],
            ['nombre' => 'Coromoro', 'slug' => 'coromoro'],
            ['nombre' => 'Sucre', 'slug' => 'sucre'],
            ['nombre' => 'Molagavita', 'slug' => 'molagavita'],
            ['nombre' => 'Los Santos', 'slug' => 'los-santos'],
            ['nombre' => 'Onzaga', 'slug' => 'onzaga'],
            ['nombre' => 'Puerto Parra', 'slug' => 'puerto-parra'],
            ['nombre' => 'El Carmen', 'slug' => 'el-carmen'],
            ['nombre' => 'Guaca', 'slug' => 'guaca'],
            ['nombre' => 'Matanza', 'slug' => 'matanza'],
            ['nombre' => 'Betulia', 'slug' => 'betulia'],
            ['nombre' => 'Aratoca', 'slug' => 'aratoca'],
            ['nombre' => 'Simacota', 'slug' => 'simacota'],
            ['nombre' => 'Guadalupe', 'slug' => 'guadalupe'],
            ['nombre' => 'Bolívar', 'slug' => 'bolivar'],
            ['nombre' => 'Güepsa', 'slug' => 'guepsa'],
            ['nombre' => 'Concepción', 'slug' => 'concepcion'],
            ['nombre' => 'Valle de San José', 'slug' => 'valle-de-san-jose'],
            ['nombre' => 'Suaita', 'slug' => 'suaita'],
            ['nombre' => 'Mogotes', 'slug' => 'mogotes'],
            ['nombre' => 'Landázuri', 'slug' => 'landazuri'],
            ['nombre' => 'Contratación', 'slug' => 'contratacion'],
            ['nombre' => 'Villanueva', 'slug' => 'villanueva'],
            ['nombre' => 'Capitanejo', 'slug' => 'capitanejo'],
            ['nombre' => 'Oiba', 'slug' => 'oiba'],
            ['nombre' => 'Barichara', 'slug' => 'barichara'],
            ['nombre' => 'Puente Nacional', 'slug' => 'puente-nacional'],
            ['nombre' => 'Zapatoca', 'slug' => 'zapatoca'],
            ['nombre' => 'Lebrija', 'slug' => 'lebrija'],
            ['nombre' => 'San Vicente de Chucurí', 'slug' => 'san-vicente-de-chucuri'],
            ['nombre' => 'Sabana de Torres', 'slug' => 'sabana-de-torres'],
            ['nombre' => 'Barbosa', 'slug' => 'barbosa'],
            ['nombre' => 'San Gil', 'slug' => 'san-gil'],
            ['nombre' => 'Girón', 'slug' => 'giron'],
            ['nombre' => 'Barrancabermeja', 'slug' => 'barrancabermeja'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'sucre'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Tolú', 'slug' => 'tolu'],
            ['nombre' => 'Sincelejo', 'slug' => 'sincelejo'],
            ['nombre' => 'San Pedro', 'slug' => 'san-pedro'],
            ['nombre' => 'San Marcos', 'slug' => 'san-marcos'],
            ['nombre' => 'Palmito', 'slug' => 'palmito'],
            ['nombre' => 'Ovejas', 'slug' => 'ovejas'],
            ['nombre' => 'Majagual', 'slug' => 'majagual'],
            ['nombre' => 'Los Palmitos', 'slug' => 'los-palmitos'],
            ['nombre' => 'Guaranda', 'slug' => 'guaranda'],
            ['nombre' => 'Coveñas', 'slug' => 'covenas'],
            ['nombre' => 'Corozal', 'slug' => 'corozal'],
            ['nombre' => 'Caimito', 'slug' => 'caimito'],
            ['nombre' => 'Buenavista', 'slug' => 'buenavista'],
            ['nombre' => 'Chalán', 'slug' => 'chalan'],
            ['nombre' => 'Colosó', 'slug' => 'coloso'],
            ['nombre' => 'La Unión', 'slug' => 'la-union'],
            ['nombre' => 'Morroa', 'slug' => 'morroa'],
            ['nombre' => 'Toluviejo', 'slug' => 'toluviejo'],
            ['nombre' => 'San Juan de Betulia', 'slug' => 'san-juan-de-betulia'],
            ['nombre' => 'Galeras', 'slug' => 'galeras'],
            ['nombre' => 'San Benito Abad', 'slug' => 'san-benito-abad'],
            ['nombre' => 'Sampués', 'slug' => 'sampues'],
            ['nombre' => 'Sucre', 'slug' => 'sucre'],
            ['nombre' => 'Sincé', 'slug' => 'since'],
            ['nombre' => 'San Onofre', 'slug' => 'san-onofre'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'tolima'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Villarrica', 'slug' => 'villarrica'],
            ['nombre' => 'Venadillo', 'slug' => 'venadillo'],
            ['nombre' => 'Suárez', 'slug' => 'suarez'],
            ['nombre' => 'San Luis', 'slug' => 'san-luis'],
            ['nombre' => 'Rovira', 'slug' => 'rovira'],
            ['nombre' => 'Rioblanco', 'slug' => 'rioblanco'],
            ['nombre' => 'Piedras', 'slug' => 'piedras'],
            ['nombre' => 'Ortega', 'slug' => 'ortega'],
            ['nombre' => 'Natagaima', 'slug' => 'natagaima'],
            ['nombre' => 'Mariquita', 'slug' => 'mariquita'],
            ['nombre' => 'Líbano', 'slug' => 'libano'],
            ['nombre' => 'Lérida', 'slug' => 'lerida'],
            ['nombre' => 'Honda', 'slug' => 'honda'],
            ['nombre' => 'Herveo', 'slug' => 'herveo'],
            ['nombre' => 'Guamo', 'slug' => 'guamo'],
            ['nombre' => 'Flandes', 'slug' => 'flandes'],
            ['nombre' => 'Espinal', 'slug' => 'espinal'],
            ['nombre' => 'Dolores', 'slug' => 'dolores'],
            ['nombre' => 'Cunday', 'slug' => 'cunday'],
            ['nombre' => 'Chaparral', 'slug' => 'chaparral'],
            ['nombre' => 'Cajamarca', 'slug' => 'cajamarca'],
            ['nombre' => 'Guayabal', 'slug' => 'guayabal'],
            ['nombre' => 'Alpujarra', 'slug' => 'alpujarra'],
            ['nombre' => 'Murillo', 'slug' => 'murillo'],
            ['nombre' => 'Planadas', 'slug' => 'planadas'],
            ['nombre' => 'San Antonio', 'slug' => 'san-antonio'],
            ['nombre' => 'Santa Isabel', 'slug' => 'santa-isabel'],
            ['nombre' => 'Villa Hermosa', 'slug' => 'villa-hermosa'],
            ['nombre' => 'Coello', 'slug' => 'coello'],
            ['nombre' => 'Valle de San Juan', 'slug' => 'valle-de-san-juan'],
            ['nombre' => 'Casabianca', 'slug' => 'casabianca'],
            ['nombre' => 'Alvarado', 'slug' => 'alvarado'],
            ['nombre' => 'Anzoátegui', 'slug' => 'anzoategui'],
            ['nombre' => 'Icononzo', 'slug' => 'icononzo'],
            ['nombre' => 'Prado', 'slug' => 'prado'],
            ['nombre' => 'Coyaima', 'slug' => 'coyaima'],
            ['nombre' => 'Roncesvalles', 'slug' => 'roncesvalles'],
            ['nombre' => 'Ataco', 'slug' => 'ataco'],
            ['nombre' => 'Ambalema', 'slug' => 'ambalema'],
            ['nombre' => 'Falan', 'slug' => 'falan'],
            ['nombre' => 'Carmen de Apicalá', 'slug' => 'carmen-de-apicala'],
            ['nombre' => 'Saldaña', 'slug' => 'saldana'],
            ['nombre' => 'Purificación', 'slug' => 'purificacion'],
            ['nombre' => 'Fresno', 'slug' => 'fresno'],
            ['nombre' => 'Melgar', 'slug' => 'melgar'],
            ['nombre' => 'Ibagué', 'slug' => 'ibague'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'valle-del-cauca'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Zarzal', 'slug' => 'zarzal'],
            ['nombre' => 'Yumbo', 'slug' => 'yumbo'],
            ['nombre' => 'Ulloa', 'slug' => 'ulloa'],
            ['nombre' => 'Trujillo', 'slug' => 'trujillo'],
            ['nombre' => 'Toro', 'slug' => 'toro'],
            ['nombre' => 'Sevilla', 'slug' => 'sevilla'],
            ['nombre' => 'Pradera', 'slug' => 'pradera'],
            ['nombre' => 'Obando', 'slug' => 'obando'],
            ['nombre' => 'La Cumbre', 'slug' => 'la-cumbre'],
            ['nombre' => 'La Candelaria', 'slug' => 'la-candelaria'],
            ['nombre' => 'El Cerrito', 'slug' => 'el-cerrito'],
            ['nombre' => 'El Cairo', 'slug' => 'el-cairo'],
            ['nombre' => 'El Águila', 'slug' => 'el-aguila'],
            ['nombre' => 'Calima', 'slug' => 'calima'],
            ['nombre' => 'Cali', 'slug' => 'cali'],
            ['nombre' => 'Buenaventura', 'slug' => 'buenaventura'],
            ['nombre' => 'Bolívar', 'slug' => 'bolivar'],
            ['nombre' => 'Ayacucho', 'slug' => 'ayacucho'],
            ['nombre' => 'Andalucía', 'slug' => 'andalucia'],
            ['nombre' => 'Alcalá', 'slug' => 'alcala'],
            ['nombre' => 'Argelia', 'slug' => 'argelia'],
            ['nombre' => 'San Pedro', 'slug' => 'san-pedro'],
            ['nombre' => 'Versalles', 'slug' => 'versalles'],
            ['nombre' => 'Barrio Canada', 'slug' => 'barrio-canada'],
            ['nombre' => 'Vijes', 'slug' => 'vijes'],
            ['nombre' => 'Ginebra', 'slug' => 'ginebra'],
            ['nombre' => 'El Dovio', 'slug' => 'el-dovio'],
            ['nombre' => 'Yotoco', 'slug' => 'yotoco'],
            ['nombre' => 'Riofrío', 'slug' => 'riofrio'],
            ['nombre' => 'Restrepo', 'slug' => 'restrepo'],
            ['nombre' => 'La Victoria', 'slug' => 'la-victoria'],
            ['nombre' => 'Dagua', 'slug' => 'dagua'],
            ['nombre' => 'Ansermanuevo', 'slug' => 'ansermanuevo'],
            ['nombre' => 'Bugalagrande', 'slug' => 'bugalagrande'],
            ['nombre' => 'Guacarí', 'slug' => 'guacari'],
            ['nombre' => 'La Unión', 'slug' => 'la-union'],
            ['nombre' => 'Roldanillo', 'slug' => 'roldanillo'],
            ['nombre' => 'Caicedonia', 'slug' => 'caicedonia'],
            ['nombre' => 'Jamundí', 'slug' => 'jamundi'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'vaupes'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'Carurú', 'slug' => 'caruru'],
            ['nombre' => 'Pacoa', 'slug' => 'pacoa'],
            ['nombre' => 'Papunahua', 'slug' => 'papunahua'],
            ['nombre' => 'Yavaraté', 'slug' => 'yavarate'],
            ['nombre' => 'Taraira', 'slug' => 'taraira'],
            ['nombre' => 'Mitú', 'slug' => 'mitu'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }

        $department = Departamento::where(['slug' => 'vichada'])->firstOrFail();

        $ciudades = [
            ['nombre' => 'La Primavera', 'slug' => 'la-primavera'],
            ['nombre' => 'Santa Rosalia', 'slug' => 'santa-rosalia'],
            ['nombre' => 'Cumaribo', 'slug' => 'cumaribo'],
            ['nombre' => 'Puerto Carreño', 'slug' => 'puerto-carreno'],
        ];

        foreach ($ciudades as $ciudad) {
            $department->ciudades()->updateOrCreate($ciudad,$ciudad);
        }
    }
}

