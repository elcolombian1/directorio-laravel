<?php

use Illuminate\Database\Seeder;
use App\Servicio;
class ServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $servicios=[
                ['nombre'=>'Salud Ocupacional'],
                ['nombre'=>'Odontologia'],
                ['nombre'=>'Belleza y cosmeticos'],
                ['nombre'=>'Mediciones ambientales'],
                ['nombre'=>'Psicologia'],
                ['nombre'=>'Ingenieria industrial'],
                ['nombre'=>'Medicina prepagada'],
                ['nombre'=>'Medicina naturista'],
            ];

            foreach ($servicios as $servicio)
            {
                Servicio::updateOrCreate($servicio);
            }
    }
}
